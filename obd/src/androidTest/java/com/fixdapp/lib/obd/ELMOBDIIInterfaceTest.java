//package com.fixdapp.lib.obd;
//
//import android.test.AndroidTestCase;
//
//import com.fixdapp.lib.ByteArray;
//import com.fixdapp.lib.obd.mock.MockELMInterface;
//
//import java.util.List;
//
///**
// * Created by charles on 12/9/15.
// */
//public class ELMOBDIIInterfaceTest extends AndroidTestCase {
//
//    MockELMInterface obdii;
//
//    public void setUp() throws Exception {
//        obdii = new MockELMInterface(false, true, MockELMInterface.VINResponseType.VALID,
//                Protocol.CAN_11_250, MockELMInterface.MileageResponseType.BOTH, false);
//        obdii.reset();
//    }
//
//    public void testParseStandardResponses() throws Exception {
//
//        List<PIDResponse> responses;
//
//        responses = obdii.sendMessage(Mode.CURRENT_DATA, PID.VEHICLE_SPEED);
//        assertEquals("supported PIDs should return data", 0x80, responses.get(0).getData().getUnsignedByte(0));
//
//        responses = obdii.sendMessage(Mode.CURRENT_DATA, PID.THROTTLE_POS_D); //unsupported PID
//        assertTrue("unsupported PIDs should return an empty set of responses", responses.isEmpty());
//
//        obdii.setHasDtcs(false);
//        assertFalse(obdii.isCelOn());
//
//        obdii.setHasDtcs(true);
//        assertTrue(obdii.isCelOn());
//
//        MonitorStatus ms = obdii.getCurrentMonitorStatus();
//        assertTrue(ms.usesSparkIgnition());
//        assertFalse(ms.usesCompressionIgnition());
//        assertTrue(ms.isTestAvailable(MonitorStatus.MonitorTest.CATALYST));
//        assertFalse(ms.isTestRunning(MonitorStatus.MonitorTest.CATALYST));
//        assertFalse(ms.isTestAvailable(MonitorStatus.MonitorTest.AC_REFRIGERANT));
//        assertTrue(ms.isTestAvailable(MonitorStatus.MonitorTest.EGR_SYSTEM));
//        assertTrue(ms.isTestRunning(MonitorStatus.MonitorTest.EGR_SYSTEM));
//    }
//
//    public void testGetDtcs() throws Exception {
//
//        DtcSet dtcSet;
//
//        obdii.setHasDtcs(false);
//        dtcSet = obdii.getStoredDtcs();
//        assertEquals(0, dtcSet.getCount());
//
//        obdii.setHasDtcs(true);
//
//        for(boolean canMode : new boolean[]{false, true}) {
//
//            obdii.setProtocol(canMode ? Protocol.CAN_11_250 : Protocol.J1850_VPW);
//            //P0100 P0200 P0300 C0300 B0200 U0100 P0101
//
//            assertEquals(7, obdii.getDtcCount());
//
//            dtcSet = obdii.getStoredDtcs();
//            assertEquals(7, dtcSet.getCount());
//            assertTrue(dtcSet.getDtcs().contains(new DTC("P0100"))); // 01 00
//            assertTrue(dtcSet.getDtcs().contains(new DTC("P0200"))); // 02 00
//            assertTrue(dtcSet.getDtcs().contains(new DTC("P0300"))); // 03 00
//            assertTrue(dtcSet.getDtcs().contains(new DTC("C0300"))); // 05 00
//            assertTrue(dtcSet.getDtcs().contains(new DTC("B0200"))); // 82 00
//            assertTrue(dtcSet.getDtcs().contains(new DTC("U0100"))); // C1 00
//            assertTrue(dtcSet.getDtcs().contains(new DTC("P0101"))); // 01 01 (from TCM)
//        }
//    }
//
//    public void testGettingSupportedPIDs() throws Exception {
//        //  first batch:
//        // BE 1B 30 13 => 1011 1110 0001 1011 0011 0000 0001 0011 (15 total)
//        //  second batch:
//        // 80 02 20 01 => 1000 0000 0000 0010 0010 0000 0000 0001 (4 total)
//        //  third batch:
//        // 44 00 00 00 => 0100 0100 0000 0000 0000 0000 0000 0000 (2 total)
//        List<PID> supported = obdii.getSupportedPIDs(Mode.CURRENT_DATA);
//
//        assertEquals("the right number of PIDs should be supported", 15 + 4 + 2, supported.size());
//        assertTrue(supported.contains(PID.MONITOR_STATUS_SINCE_CLEARED));
//        assertFalse(supported.contains(PID.FREEZE_DTC));    //0x02
//        assertFalse(supported.contains(PID.FUEL_PRESSURE)); //0x0A
//        assertTrue(supported.contains(PID.RUNTIME_SINCE_ENGINE_START)); //0x1F
//
//        // 48 18 00 00 =>
//        // 0100 1000 0001 1000 0000 0000 0000 0000
//        supported = obdii.getSupportedPIDs(Mode.FREEZE_DATA);
//        assertEquals("the right number of PIDs should be supported", 4, supported.size());
//        assertFalse(supported.contains(PID.MONITOR_STATUS_SINCE_CLEARED));
//        assertTrue(supported.contains(PID.FREEZE_DTC));
//        assertTrue(supported.contains(PID.ENGINE_RPM)); //0x0C
//
//        //54 40 00 00 =>
//        // 0101 0100 0100 0000 0000 0000 0000 0000
//        supported = obdii.getSupportedPIDs(Mode.VEHICLE_INFO);
//        assertEquals("the right number of PIDs should be supported", 4, supported.size());
//        assertTrue(supported.contains(PID.VIN));
//        assertTrue(supported.contains(PID.CALIBRATION_ID));
//        assertFalse(supported.contains(PID.VIN_MESSAGE_COUNT));
//        assertFalse(supported.contains(PID.CALIBRATION_ID_MESSAGE_COUNT));
//        assertTrue(supported.contains(PID.ECU_NAME));
//    }
//
//    public void testGettingFreezeFrames() throws Exception {
//        FreezeFrameSet ff = obdii.getCurrentFreezeFrame();
//
//        assertEquals(obdii.getSupportedPIDs(Mode.CURRENT_DATA).size(), ff.getResponseMap().size());
//        assertEquals(0x80, ff.getResponseMap().get(PID.VEHICLE_SPEED).get(0).getData().getUnsignedByte(0));
//        assertEquals(0x3C, ff.getResponseMap().get(PID.FTRIM_SHORT_1).get(0).getData().getUnsignedByte(0));
//        assertEquals(0x46, ff.getResponseMap().get(PID.FTRIM_LONG_1).get(0).getData().getUnsignedByte(0));
//
//        FreezeFrameSet ff2 = obdii.getDTCFreezeFrame(0);
//
//        assertEquals(obdii.getSupportedPIDs(Mode.FREEZE_DATA).size(), ff2.getResponseMap().size());
//
//        ByteArray ba = ff2.getResponseMap().get(PID.FREEZE_DTC).get(0).getData();
//        assertEquals("P0107", OBDIIInterface.unsignedShortToDtcCode(ba, 0));
//
//        assertEquals(5000, ff2.getResponseMap().get(PID.ENGINE_RPM).get(0).getData().getUnsignedShort(0)/4);
//        assertEquals(120, ff2.getResponseMap().get(PID.VEHICLE_SPEED).get(0).getData().getUnsignedByte(0));
//
//        assertEquals(0, obdii.getDTCFreezeFrame(1).getResponseMap().size());
//    }
//
//    public void testCANVinResponse() throws Exception {
//        assertEquals("1G1JC5444R7252367", obdii.getVin());
//    }
//
//    public void testUnavailableVin() throws Exception {
//
//        obdii.setResponse("0902", "NO DATA");
//        assertNull(obdii.getVin());
//    }
//
//    public void testNonCANVinResponse() throws Exception {
//        obdii.setProtocol(Protocol.J1850_VPW);
//        assertEquals("1G1JC5444R7252367", obdii.getVin());
//    }
//
//    public void testBufferFullResponse() throws Exception {
//        obdii.setProtocol(Protocol.CAN_11_250);
//        //actual response from ECUSim 2000. Note 8 instead of 18 on line 1 and Buffer full on last line
////        obdii.setResponse("090A",
////                " 8 DA F1 10 10 17 49 0A 01 45 43 55\r" +
////                        "18 DA F1 10 21 31 2D 45 6E 67 69 6E\r" +
////                        "18 DA F1 10 22 65 43 6F 6E 74 72 6F\r" +
////                        "18 DA F1 10 23 6C 2D 45 00 00 00 00\r" +
////                        "18 DA F1 18 10 17 49 0A 01 45 43 55\r" +
////                        "18 DA F1 18 21 32 2D 54 72 61 6E 73\r" +
////                        "18 DA F1 18 22 6D 69 73 73 69 6F 6E\r" +
////                        "18 DA F1 18 23 6C 2D 45 00 0BUFFER FULL");
////
////        List<PIDResponse> responses = obdii.sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME);
////        assertEquals(2, responses.size());                                                                      //TODO fails because fromMultiline() throws InvalidResponseException
////        assertEquals("ECU1-EngineControl-E", responses.get(0).getData().getAsASCIIString(obdii.getProtocol())); //TODO fails because of mismatching header
////        assertEquals("ECU2-Transmissionl-E", responses.get(1).getData().getAsASCIIString(obdii.getProtocol())); //TODO fails because of "BUFFER FULL"
//
//        //we can get around this by requesting which address to get data from
//        obdii.setResponse("090A",
//                "18 DA F1 10 10 17 49 0A 01 45 43 55\r" +
//                "18 DA F1 10 21 31 2D 45 6E 67 69 6E\r" +
//                "18 DA F1 10 22 65 43 6F 6E 74 72 6F\r" +
//                "18 DA F1 10 23 6C 2D 45 00 00 00 00");
//        List<PIDResponse> responses = obdii.sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME, "18 DA F1 10", null);
//        assertEquals(1, responses.size());
//        assertEquals("ECU1-EngineControl-E", responses.get(0).getData().getAsASCIIString().replaceAll("\0", ""));
//
//        obdii.setResponse("090A",
//                "18 DA F1 18 10 17 49 0A 01 45 43 55\r" +
//                "18 DA F1 18 21 32 2D 54 72 61 6E 73\r" +
//                "18 DA F1 18 22 6D 69 73 73 69 6F 6E\r" +
//                "18 DA F1 18 23 6C 2D 45 00 00 00 00");
//        responses = obdii.sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME, "18 DA F1 18", null);
//        assertEquals(1, responses.size());
//        assertEquals("ECU2-Transmissionl-E", responses.get(0).getData().getAsASCIIString().replaceAll("\0", ""));
//
//        assertEquals("there should be no responses for the wrong address", 0, obdii.sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME, "18 DA F1 10", null).size());
//
//        obdii.setResponse("090A",
//                "18 DA F1 10 10 17 49 0A 01 45 43 55\r" +
//                "18 DA F1 10 21 31 2D 45 6E 67 69 6E\r" +
//                "18 DA F1 10 22 65 43 6F 6E 74 72 6F\r" +
//                "18 DA F1 10 23 6C 2D 45 00 00 00 00\r" +
//
//                "18 DA F1 18 10 17 49 0A 01 45 43 55\r" +
//                "18 DA F1 18 21 32 2D 54 72 61 6E 73\r" +
//                "18 DA F1 18 22 6D 69 73 73 69 6F 6E\r" +
//                "18 DA F1 18 23 6C 2D 45 00 00 00 00");
//        assertEquals("there should be only responses for the requested address", "18DAF118", obdii.sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME, "18 DA F1 18", null).get(0).getResponderId());
//        assertEquals("there should be only responses for the requested address", "18DAF110", obdii.sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME, "18 DA F1 10", null).get(0).getResponderId());
//    }
//}
