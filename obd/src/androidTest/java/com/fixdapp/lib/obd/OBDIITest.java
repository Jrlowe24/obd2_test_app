package com.fixdapp.lib.obd;

import android.test.AndroidTestCase;

/**
 * Created by charles on 12/7/15.
 */
public class OBDIITest extends AndroidTestCase {

    public void testPIDGet() throws Exception {
        assertEquals("should return correct pid for mode 1", PID.O2_VOLTAGE_FTRIM_SHORT_1_2, PID.get(Mode.CURRENT_DATA, (byte) 0x15));
        assertEquals("should return same pid for mode 2", PID.O2_VOLTAGE_FTRIM_SHORT_1_2, PID.get(Mode.FREEZE_DATA, (byte) 0x15));
        assertEquals("should get pid for mode 3", PID.GET_DTCS, PID.get(Mode.STORED_DTCS, (byte) 0x00));
        assertNull("should not return pid for other mode 3 codes", PID.get(Mode.STORED_DTCS, (byte) 0x15));

        assertEquals("should return one pid for mode 9", PID.ECU_NAME, PID.get(Mode.VEHICLE_INFO, (byte) 0x0A));
        assertEquals("...and a different pid for the same code on mode 2", PID.FUEL_PRESSURE, PID.get(Mode.FREEZE_DATA, (byte) 0x0A));

        assertNull("mode 1 can't do pid 2", PID.get(Mode.CURRENT_DATA, (byte) 0x02));
        assertNull("mode 2 can't do pid 1", PID.get(Mode.FREEZE_DATA, (byte) 0x01));
    }
}
