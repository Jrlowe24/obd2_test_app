package com.fixdapp.lib.obd;

import android.test.AndroidTestCase;

import junit.framework.Assert;

/**
 * Created by User on 9/29/15.
 */
public class VinValidatorTest extends AndroidTestCase {

    public void testVins(){

        VinValidator v = new VinValidator(getContext());

        // fred's car
        String v1 = "1gkes63m062258716";
        assertTrue(v.isValidVin(v1));
        Assert.assertEquals(2006, v.getYear(v1));

        //real VIN for a US-bought car, on edmunds
        String v2 = "1FTRW08LX3KB67611";
        assertTrue(v.isValidVin(v2));
        Assert.assertEquals(2003, v.getYear(v2));
        Assert.assertEquals("North America", v.getRegion(v2));
        Assert.assertEquals("United States", v.getCountry(v2));
        Assert.assertEquals("Ford", v.getMake(v2));

        // customer-reported VIN, doesn't pass checksum
        String v3 = "1dfee1460vha96843";
        assertFalse(v.isValidVin(v3));

        //customer reported vin
        String v4 = "WBABW33474PL33765";
        assertTrue(v.isValidVin(v4));
        Assert.assertEquals("BMW", v.getMake(v4));
        Assert.assertEquals(2004, v.getYear(v4));

        // real VIN for a US-bought car NOT on edmunds
        String v5 = "1FTSX30Y38ED11416";
        assertTrue(v.isValidVin(v2));
        Assert.assertEquals(2003, v.getYear(v2));
        Assert.assertEquals("Ford", v.getMake(v2));
    }
}
