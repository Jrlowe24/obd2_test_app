package com.fixdapp.lib.sensor.bluetooth.classic

import android.os.Looper
import com.fixdapp.lib.sensor.DataInterface
import okio.*
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

/**
 * Created by cjk on 2/28/17.
 *
 * A {@link DataInterface} which uses input and output streams. Subscription is blocking!
 */
class StreamDataInterface(inputStream: InputStream, outputStream: OutputStream) : DataInterface {

    val source: BufferedSource = Okio.buffer(Okio.source(inputStream))
    val sink: BufferedSink = Okio.buffer(Okio.sink(outputStream))

    override fun write(data: ByteString, terminator: Byte): ByteString {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw IllegalThreadStateException("Do not connect to socket on main thread")
        }
        sink.write(data)
        sink.flush()
        val out = Buffer()
        do {
            val b = source.readByte()
            out.writeByte(b.toInt())
        } while (b != terminator)

        return out.readByteString()
    }

    override fun close() {
        try {
            source.close()
        }catch (e: IOException) {
            // no biggie, likely already closed
        }
        try {
            sink.close()
        }catch (e: IOException) {
            // no biggie, likely already closed
        }
    }
}
