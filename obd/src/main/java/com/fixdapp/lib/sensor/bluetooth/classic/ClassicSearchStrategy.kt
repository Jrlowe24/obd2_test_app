package com.fixdapp.lib.sensor.bluetooth.classic

import android.bluetooth.BluetoothDevice
import com.fixdapp.android.logger.LogManager
import com.fixdapp.lib.sensor.DeviceMatcher
import com.fixdapp.lib.sensor.bluetooth.BluetoothBroadcastReceiver
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor
import com.fixdapp.lib.sensor.bluetooth.SearchStrategy
import rx.Emitter
import rx.Observable

/**
 * Created by cjk on 3/1/17.
 */
internal class ClassicSearchStrategy(
        val deviceMatcher: DeviceMatcher,
        val bluetoothBroadcastReceiver: BluetoothBroadcastReceiver,
        private val logManager: LogManager
): SearchStrategy {

    private val logger = logManager.newLogger

    private var hasSeenStart = false

    override fun scan(): Observable<BluetoothSensor> {
        // range + concatMap allows us to repeat the scan indefinitely
        return Observable.range(0, Int.MAX_VALUE).concatMap { scanForAll() }
                .doOnNext { logger.i("Found device (classic search):", it.name, it.address) }
                .filter { deviceMatcher.matches(it) }
                .map { device -> ClassicBluetoothSensor(device, bluetoothBroadcastReceiver, logManager) }
    }

    private fun scanForAll(): Observable<BluetoothDevice> {
        return Observable.create<BluetoothDevice>({ emitter ->
            logger.d("scan started")

            if (bluetoothBroadcastReceiver.adapter == null) {
                emitter.onError(BluetoothException.Unavailable())
                return@create
            }

            bluetoothBroadcastReceiver.adapter.cancelDiscovery()
            bluetoothBroadcastReceiver.deviceDiscoveredCallback = emitter::onNext
            hasSeenStart = false
            bluetoothBroadcastReceiver.scanChangeCallback = { scanning ->
                logger.d("scan state changed", scanning, hasSeenStart)
                if (scanning && !hasSeenStart) {
                    hasSeenStart = true
                }else if (!scanning && hasSeenStart) {
                    logger.d("completing scan")
                    emitter.onCompleted()
                }
            }
            bluetoothBroadcastReceiver.adapter.startDiscovery()

            emitter.setCancellation {
                logger.d("scan canceled")
                bluetoothBroadcastReceiver.deviceDiscoveredCallback = null
                bluetoothBroadcastReceiver.scanChangeCallback = null
                bluetoothBroadcastReceiver.adapter.cancelDiscovery()
            }
        }, Emitter.BackpressureMode.BUFFER)
    }
}