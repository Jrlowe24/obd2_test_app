package com.fixdapp.lib.sensor

import android.bluetooth.BluetoothDevice
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensorService
import java.util.*

/**
 * Created by cjk on 3/2/17.
 */
class FIXDSensorMatcher: DeviceMatcher {

    private enum class SensorModel(val deviceName: String, val addressPatterns: List<String>, val leServiceUUID: UUID?) {

        OLD("OBDII", listOf("00:0D:18:00:00:01"), null),
        // 00:11:67:11:14:22  00:19:5D:F4:10:A2  00:19:5D:F4:11:57  00:11:67:11:14:2E
        SETOSMART("FIXD", listOf("00:11:67:11:XX:XX", "00:19:5D:F4:XX:XX"), BluetoothSensorService.shortStringToUUID("18F0")),
        // 88:1B:99:04:73:3D  88:1B:99:08:06:50  88:1B:99:08:C3:F3  88:1B:99:11:14:9A
        // 8C:DE:52:C2:39:06  8C:DE:52:C2:74:5D  8C:DE:52:C2:A9:8A
        VIACAR("FIXD", listOf("88:1B:99:XX:XX:XX", "8C:DE:52:XX:XX:XX"), BluetoothSensorService.shortStringToUUID("FFF0"));

        fun matches(device: BluetoothDevice): Boolean {
            return deviceName == device.name || (device.name == null && matchMacAddresses(addressPatterns, device.address))
        }

        private fun matchMacAddresses(refs: List<String>, mac: String): Boolean {
            return refs.any { ref ->
                (0..ref.length - 1).none { ref[it] != 'X' && ref[it] != mac[it] }
            }
        }
    }

    override val leServiceUUIDs: List<UUID>
        get() = SensorModel.values().map { it.leServiceUUID }.filterNotNull()

    override fun matches(device: BluetoothDevice): Boolean {
        return SensorModel.values().any { it.matches(device) }
    }
}