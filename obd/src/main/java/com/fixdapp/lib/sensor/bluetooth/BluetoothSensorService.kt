package com.fixdapp.lib.sensor.bluetooth

import android.content.Context
import android.os.Build
import android.os.Looper
import com.fixdapp.android.logger.LogManager
import com.fixdapp.lib.sensor.DeviceMatcher
import com.fixdapp.lib.sensor.SensorService
import com.fixdapp.lib.sensor.bluetooth.classic.BluetoothException
import com.fixdapp.lib.sensor.bluetooth.classic.ClassicSearchStrategy
import com.fixdapp.lib.sensor.bluetooth.le.BLEStrategy
import rx.Observable
import java.util.*

/**
 * Created by cjk on 3/1/17.
 */
class BluetoothSensorService(private val deviceMatcher: DeviceMatcher,
                             private val logManager: LogManager = LogManager.getInstance()): SensorService {

    companion object Utils {
        fun shortStringToUUID(shortString: String): UUID {
            if (shortString.length != 4) throw IllegalArgumentException("shortString must be 4 characters")
            return UUID.fromString("0000$shortString-0000-1000-8000-00805F9B34FB")
        }
    }

    private val bluetoothBroadcastReceiver = BluetoothBroadcastReceiver(logManager)

    private val strategy: SearchStrategy by lazy {
        ClassicSearchStrategy(deviceMatcher, bluetoothBroadcastReceiver, logManager)
    }

    override fun register(context: Context) {
        guardMainThread()
        bluetoothBroadcastReceiver.register(context)
    }

    override fun unregister(context: Context) {
        guardMainThread()
        bluetoothBroadcastReceiver.unregister(context)
    }

    override val bluetoothState: Observable<BluetoothState>
        get() = bluetoothBroadcastReceiver.bluetoothState.toObservable()

    override val currentBluetoothState: BluetoothState
        get() = bluetoothBroadcastReceiver.bluetoothState.value

    override fun getSensors(): Observable<BluetoothSensor> {
        if (bluetoothBroadcastReceiver.adapter == null) {
            return Observable.error(BluetoothException.Unavailable())
        }
        if (bluetoothBroadcastReceiver.bluetoothState.value == BluetoothState.poweredOff) {
            return Observable.error(BluetoothException.Disabled())
        }

        return bluetoothBroadcastReceiver.bluetoothState.toObservable()
                .filter { it == BluetoothState.poweredOn }
                .take(1)
                .flatMap { strategy.scan() }
                .distinct()
    }

    private fun guardMainThread() {
        if(Looper.myLooper() != Looper.getMainLooper())
            throw IllegalStateException("Only call from main thread")
    }
}
