package com.fixdapp.lib.sensor.mock

import android.content.Context
import com.fixdapp.lib.obd.OBDIIInterface
import com.fixdapp.lib.obd.mock.MockELMInterface
import com.fixdapp.lib.sensor.SensorService
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor
import com.fixdapp.lib.sensor.bluetooth.BluetoothState
import rx.Observable
import java.util.concurrent.TimeUnit

/**
 * Created by cjk on 3/2/17.
 */
class MockSensorService(val interfaceBuilder: MockELMInterface.Builder, val simulateSearchTime: Boolean): SensorService {

    override val bluetoothState: Observable<BluetoothState>
        get() = Observable.never<BluetoothState>().startWith(BluetoothState.poweredOn)

    override val currentBluetoothState: BluetoothState
        get() = BluetoothState.poweredOn

    override fun getSensors(): Observable<BluetoothSensor> {
        return Observable.just<BluetoothSensor>(MockSensor(interfaceBuilder))
                .delay(if(simulateSearchTime) 5L else 0L, TimeUnit.SECONDS)
    }

    override fun register(context: Context) {

    }

    override fun unregister(context: Context) {

    }

    class MockSensor(val interfaceBuilder: MockELMInterface.Builder): BluetoothSensor {
        override val name: String? get() = "MOCK"

        override val address: String get() = "AA:BB:CC:DD:EE:FF"

        override val rssi: Observable<Short> get() = Observable.interval(1, TimeUnit.SECONDS).map { (-1 * java.util.Random().nextInt(100)).toShort() }

        override fun getInterface(): Observable<OBDIIInterface> {
            return Observable.fromCallable<OBDIIInterface> { interfaceBuilder.build() }
        }
    }
}