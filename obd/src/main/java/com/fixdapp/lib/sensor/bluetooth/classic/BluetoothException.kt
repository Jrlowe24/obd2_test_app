package com.fixdapp.lib.sensor.bluetooth.classic

/**
 * Created by cjk on 3/2/17.
 */
sealed class BluetoothException(message: String? = null, cause: Throwable? = null) : Exception(message, cause) {
    class Unavailable: BluetoothException()
    class Disabled: BluetoothException()
    class DeviceNoLongerConnected(cause: Throwable): BluetoothException(cause = cause)
    class ScanFailed(val code: Int): BluetoothException(message = "Scan failed. Error code: $code")
    class LeNotSupported: BluetoothException()
}