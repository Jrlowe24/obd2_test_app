package com.fixdapp.lib.sensor

import android.bluetooth.BluetoothDevice
import java.util.*

/**
 * Created by cjk on 8/25/16.
 */

interface DeviceMatcher {
    fun matches(device: BluetoothDevice): Boolean

    val leServiceUUIDs: List<UUID>
}
