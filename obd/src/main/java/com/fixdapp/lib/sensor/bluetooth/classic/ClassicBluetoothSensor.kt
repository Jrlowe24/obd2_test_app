package com.fixdapp.lib.sensor.bluetooth.classic

import android.bluetooth.BluetoothDevice
import android.os.Looper
import com.fixdapp.android.logger.LogManager
import com.fixdapp.lib.obd.ELMOBDIIInterface
import com.fixdapp.lib.obd.OBDIIInterface
import com.fixdapp.lib.sensor.bluetooth.BluetoothBroadcastReceiver
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensorService
import rx.Observable
import rx.lang.kotlin.filterNotNull
import java.io.IOException

/**
 * Created by cjk on 3/1/17.
 */
internal class ClassicBluetoothSensor(
        val device: BluetoothDevice,
        bluetoothBroadcastReceiver: BluetoothBroadcastReceiver,
        private val logManager: LogManager
): BluetoothSensor {

    private val logger = logManager.newLogger

    companion object {
        val classicSerialUUID = BluetoothSensorService.shortStringToUUID("1101")
    }

    override val name: String?
        get() = device.name

    override val address: String
        get() = device.address

    override val rssi: Observable<Short> = bluetoothBroadcastReceiver.rssiUpdated.map { it[device] }.filterNotNull()

    override fun getInterface(): Observable<OBDIIInterface> {
        return Observable.defer<OBDIIInterface> {
            logger.d("connecting to", device.name, device.address)
            if (Looper.myLooper() == Looper.getMainLooper()) {
                return@defer Observable.error(IllegalThreadStateException("Do not connect to socket on main thread"))
            }
            try {
                val socket = device.createInsecureRfcommSocketToServiceRecord(classicSerialUUID)
                socket.connect()
                val dataInterface = StreamDataInterface(socket.inputStream, socket.outputStream)
                Observable.just(ELMOBDIIInterface(dataInterface, logManager))
            }catch (e: IOException) {
                Observable.error(BluetoothException.DeviceNoLongerConnected(e))
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        val otherSensor = other as? ClassicBluetoothSensor ?: return false
        return otherSensor.device == device
    }

    override fun hashCode(): Int {
        return device.hashCode()
    }
}