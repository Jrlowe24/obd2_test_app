package com.fixdapp.lib.sensor

import okio.ByteString


/**
 * Created by cjk on 2/28/17.

 * An interface that writes to a channel, reads from it until it sees the terminate character,
 * and returns all read data (as a single)
 */
interface DataInterface {

    fun write(data: ByteString, terminator: Byte): ByteString

    fun close()
}
