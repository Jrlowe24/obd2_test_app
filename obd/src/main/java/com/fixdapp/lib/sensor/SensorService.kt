package com.fixdapp.lib.sensor

import android.content.Context
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor
import com.fixdapp.lib.sensor.bluetooth.BluetoothState
import com.fixdapp.lib.sensor.bluetooth.classic.BluetoothException
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * Created by cjk on 3/1/17.
 */
interface SensorService {

    val bluetoothState: Observable<BluetoothState>

    val currentBluetoothState: BluetoothState

    fun getSensors(): Observable<BluetoothSensor>

    fun register(context: Context)

    fun unregister(context: Context)

    fun sensorsInRange(): Observable<List<BluetoothSensor>> {

//        // every 10 seconds, do a fresh scan and collect all the items into a list
//        // keep track of who was in the last set, and return the combination of the two
//        val interval = Observable.interval(10, TimeUnit.SECONDS).share()
//
//        // [] [A] [AB]
//        val scanAndAccumulate = getSensors().scan(emptyList<BluetoothSensor>(), { a, b -> a + b })
//
//        // * [] [A] [AB] * [] [B] [AB] * [] [A] [AC] [ACB] * [] [A] [B] * []
//        val intervalScan = interval.startWith(0).switchMap { scanAndAccumulate }.share()
//
//        //               * [AB]        * [AB]              * [ACB]      * [AB]
//        val previousScan = interval.withLatestFrom(intervalScan) { _, lastScanSet -> lastScanSet }.startWith(emptyList<BluetoothSensor>())
//
//        // * [] [A] [AB] * [AB][AB][AB]* [AB][AB][ABC][ABC]* [ACB][A][B] * [AB]
//        return Observable.combineLatest(intervalScan, previousScan) { discovered, previousWindow ->
//            (discovered + previousWindow).toSet()
//        }.distinctUntilChanged().map { it.toList() }

        // every 10 seconds, do a fresh scan and collect all the items into a list, except for the
        // initial interval, where we emit all new ones immediately
        val interval = Observable.interval(10, TimeUnit.SECONDS)
        return interval.switchMap { getSensors() }.buffer(interval.skip(1))
                .mergeWith(getSensors().scan(emptyList<BluetoothSensor>(), { a, b -> a + b }).takeUntil(interval))
    }
}