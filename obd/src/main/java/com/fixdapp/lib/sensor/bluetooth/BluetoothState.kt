package com.fixdapp.lib.sensor.bluetooth

/**
 * Created by cjk on 3/1/17.
 */
enum class BluetoothState {
    unknown,
    unavailable,
    poweredOff,
    poweredOn
}