package com.fixdapp.lib.sensor.bluetooth

import com.fixdapp.lib.obd.OBDIIInterface
import rx.Observable

/**
 * Created by cjk on 3/1/17.
 */
interface BluetoothSensor {

    val name: String?

    val address: String

    val rssi: Observable<Short>

    fun getInterface(): Observable<OBDIIInterface>
}