package com.fixdapp.lib.sensor.bluetooth.le

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.os.Build
import android.os.ParcelUuid
import com.fixdapp.android.logger.LogManager
import com.fixdapp.lib.sensor.DeviceMatcher
import com.fixdapp.lib.sensor.bluetooth.BluetoothBroadcastReceiver
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor
import com.fixdapp.lib.sensor.bluetooth.SearchStrategy
import com.fixdapp.lib.sensor.bluetooth.classic.BluetoothException
import com.fixdapp.lib.sensor.bluetooth.classic.ClassicBluetoothSensor
import rx.Emitter
import rx.Observable

/**
 * Created by cjk on 3/1/17.
 */
internal class BLEStrategy(
        val deviceMatcher: DeviceMatcher,
        val bluetoothBroadcastReceiver: BluetoothBroadcastReceiver,
        val logManager: LogManager
): SearchStrategy {

    companion object {
        internal interface LeScanner {
            fun scanForAll(adapter: BluetoothAdapter): Observable<BluetoothDevice>
        }
    }

    @SuppressLint("NewApi")
    override fun scan(): Observable<BluetoothSensor> {
        if (bluetoothBroadcastReceiver.adapter == null) {
            return Observable.error(BluetoothException.Unavailable())
        }
        return when {
            Build.VERSION.SDK_INT < 18 -> return Observable.error(BluetoothException.LeNotSupported())
            Build.VERSION.SDK_INT >= 21 -> NewLeScanner(deviceMatcher, logManager)
            else -> OldLeScanner(deviceMatcher)
        }.scanForAll(bluetoothBroadcastReceiver.adapter)
                .filter { deviceMatcher.matches(it) }
                .map { ClassicBluetoothSensor(it, bluetoothBroadcastReceiver, logManager) }
    }

    @TargetApi(18)
    internal class OldLeScanner(val deviceMatcher: DeviceMatcher): LeScanner {

        @Suppress("DEPRECATION")
        override fun scanForAll(adapter: BluetoothAdapter): Observable<BluetoothDevice> {
            return Observable.create({ emitter ->
                val callback = BluetoothAdapter.LeScanCallback { device, _, _ -> emitter.onNext(device) }
                adapter.startLeScan(deviceMatcher.leServiceUUIDs.toTypedArray(), callback)
                emitter.setCancellation { adapter.stopLeScan(callback) }
            }, Emitter.BackpressureMode.BUFFER)
        }
    }

    @TargetApi(21)
    internal class NewLeScanner(val deviceMatcher: DeviceMatcher, val logManager: LogManager): LeScanner {

        val filters: List<ScanFilter>
            get() = deviceMatcher.leServiceUUIDs.map { uuid ->
                ScanFilter.Builder().setServiceUuid(ParcelUuid(uuid)).build()
            }

        override fun scanForAll(adapter: BluetoothAdapter): Observable<BluetoothDevice> {
            return Observable.create<BluetoothDevice>({ emitter ->
                // emitter.onError(UnsupportedOperationException("Bluetooth LE not supported"))
                // currently erroring will cause all scans to fail so return no elements instead
                val scanner = adapter.bluetoothLeScanner ?: return@create emitter.onCompleted().also {
                    logManager.getNewLogger("BLE").w("Bluetooth LE not supported")
                }

                val callback = object: ScanCallback() {

                    override fun onScanFailed(errorCode: Int) {
                        super.onScanFailed(errorCode)
                        emitter.onError(BluetoothException.ScanFailed(errorCode))
                    }

                    override fun onScanResult(callbackType: Int, result: ScanResult?) {
                        super.onScanResult(callbackType, result)
                        if (result != null) emitter.onNext(result.device)
                    }
                }
                scanner.startScan(filters, ScanSettings.Builder().build(), callback)

                emitter.setCancellation { scanner.stopScan(callback) }
            }, Emitter.BackpressureMode.BUFFER)
        }
    }
}