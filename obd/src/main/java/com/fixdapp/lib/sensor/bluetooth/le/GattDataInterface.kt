//package com.fixdapp.lib.sensor.bluetooth.le
//
//import android.annotation.TargetApi
//import android.bluetooth.BluetoothDevice
//import android.bluetooth.BluetoothGatt
//import android.bluetooth.BluetoothGattCallback
//import android.bluetooth.BluetoothGattCharacteristic
//import android.content.Context
//import co.metalab.asyncawait.async
//import co.metalab.asyncawait.await
//import com.fixdapp.lib.BitMask
//import com.fixdapp.lib.sensor.DataInterface
//import rx.AsyncEmitter
//import rx.Observable
//import rx.lang.kotlin.PublishSubject
//import java.io.ByteArrayOutputStream
//import java.util.*
//
///**
// * Created by cjk on 3/1/17.
// */
//@Deprecated("Untested")
//@TargetApi(18)
//class GattDataInterface private constructor(
//        val gatt: BluetoothGatt,
//        val readCharacteristic: BluetoothGattCharacteristic,
//        val writeCharacteristic: BluetoothGattCharacteristic,
//        val callback: Callback
//): BluetoothGattCallback(), DataInterface {
//
//    companion object Factory {
//
//        fun connect(context: Context, device: BluetoothDevice, serviceUUID: UUID,
//                    readUUID: UUID, writeUUID: UUID): Observable<GattDataInterface> {
//
//            val callback = Callback()
//            device.connectGatt(context, true, callback)
//            return callback.connected.flatMap { gatt ->
//                gatt.discoverServices()
//                callback.servicesDiscovered.asObservable().take(1)
//            }.flatMap { gatt ->
//                val service = gatt.getService(serviceUUID)
//                        ?: return@flatMap Observable.empty<GattDataInterface>()
//                val readCharacteristic = service.getCharacteristic(readUUID)
//                        ?: return@flatMap Observable.empty<GattDataInterface>()
//                val writeCharacteristic = service.getCharacteristic(writeUUID)
//                        ?: return@flatMap Observable.empty<GattDataInterface>()
//
//                val readProps = BitMask(readCharacteristic.properties)
//                val writeProps = BitMask(writeCharacteristic.properties)
//                if (!readProps.contains(BluetoothGattCharacteristic.PROPERTY_NOTIFY))
//                    return@flatMap Observable.empty<GattDataInterface>()
//                if (!writeProps.contains(BluetoothGattCharacteristic.PROPERTY_WRITE)
//                        && !writeProps.contains(BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) {
//                    return@flatMap Observable.empty<GattDataInterface>()
//                }
//                val iface = GattDataInterface(gatt, readCharacteristic, writeCharacteristic, callback)
//                return@flatMap Observable.just(iface)
//            }
//        }
//
//        private class Callback: BluetoothGattCallback() {
//
//            val connected = PublishSubject<BluetoothGatt>()
//            val servicesDiscovered = PublishSubject<BluetoothGatt>()
//            val characteristicChanged = PublishSubject<ByteArray>()
//            val characteristicWrite = PublishSubject<ByteArray>()
//
//            override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
//                super.onConnectionStateChange(gatt, status, newState)
//                if (gatt != null && newState == BluetoothGatt.STATE_CONNECTED) {
//                    connected.onNext(gatt)
////                } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
////                    connected.onError(BluetoothException.DeviceDisconnected())
//                }
//            }
//
//            override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
//                super.onServicesDiscovered(gatt, status)
//                if (gatt != null) servicesDiscovered.onNext(gatt)
//            }
//
//            override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
//                super.onCharacteristicChanged(gatt, characteristic)
//                if (characteristic != null) characteristicChanged.onNext(characteristic.value)
//            }
//
//            override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
//                super.onCharacteristicWrite(gatt, characteristic, status)
//                if (characteristic != null) characteristicWrite.onNext(characteristic.value)
//            }
//        }
//    }
//
//    override fun write(data: ByteArray, terminator: Byte): Observable<ByteArray> {
//        return Observable.fromAsync({ emitter ->
//            async {
//                gatt.setCharacteristicNotification(readCharacteristic, true)
//                writeCharacteristic.value = data
//                gatt.writeCharacteristic(writeCharacteristic)
//                await(callback.characteristicWrite.asObservable())
//                val buffer = ByteArrayOutputStream()
//                do {
//                    val read = await(callback.characteristicChanged.asObservable())
//                    buffer.write(read)
//                } while (!buffer.toByteArray().contains(terminator))
//
//                gatt.setCharacteristicNotification(readCharacteristic, false)
//                emitter.onNext(buffer.toByteArray())
//                emitter.onCompleted()
//            }
//            emitter.setCancellation {
//                gatt.setCharacteristicNotification(readCharacteristic, false)
//                async.cancelAll()
//            }
//        }, AsyncEmitter.BackpressureMode.BUFFER)
//    }
//
//    override fun close() {
//        async.cancelAll()
//    }
//}