package com.fixdapp.lib.sensor.bluetooth

import rx.Observable

/**
 * Created by cjk on 3/1/17.
 */
internal interface SearchStrategy {

    fun scan(): Observable<BluetoothSensor>
}