package com.fixdapp.lib.sensor.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.fixdapp.android.logger.LogManager
import rx.Observable
import rx.extensions.Variable
import rx.subjects.BehaviorSubject
import rx.subjects.PublishSubject

/**
 * Created by cjk on 2/28/17.
 */
internal class BluetoothBroadcastReceiver(logManager: LogManager): BroadcastReceiver() {

    val adapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    val bluetoothState = Variable<BluetoothState>(BluetoothState.unknown)

    var deviceDiscoveredCallback: ((BluetoothDevice) -> Unit)? = null
    var scanChangeCallback: ((Boolean) -> Unit)? = null

    val deviceDisconnected: PublishSubject<BluetoothDevice> = PublishSubject.create()

    private val _rssiUpdated: BehaviorSubject<Map<BluetoothDevice, Short>> = BehaviorSubject.create(emptyMap())

    val rssiUpdated: Observable<Map<BluetoothDevice,Short>> get() = _rssiUpdated.asObservable()

    private val logger = logManager.newLogger

    init {
        if (adapter == null) {
            bluetoothState.value = BluetoothState.unavailable
        } else {
            when (adapter.state) {
                BluetoothAdapter.STATE_OFF -> bluetoothState.value = BluetoothState.poweredOff
                BluetoothAdapter.STATE_ON -> bluetoothState.value = BluetoothState.poweredOn
            }
        }
    }

    fun register(context: Context) {
        val intentFilter = IntentFilter()
        intentFilter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED)
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
        intentFilter.addAction(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND)
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)

        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)

        context.registerReceiver(this, intentFilter)
    }

    fun unregister(context: Context) {
        context.unregisterReceiver(this)
    }

//    fun cleanUp() {
//        _rssiUpdated.onCompleted()
//        deviceDisconnected.onCompleted()
//    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent == null) {
            return
        }
        val action = intent.action
        logger.d("Bluetooth event received:", action)
        when (action) {

            BluetoothAdapter.ACTION_STATE_CHANGED -> {
                val currentState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                val previousState = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, -1)
                logger.i("State change:", bluetoothAdapterConstantsToString(previousState),
                        "to", bluetoothAdapterConstantsToString(currentState))

                when (currentState) {
                    BluetoothAdapter.STATE_OFF -> bluetoothState.value = BluetoothState.poweredOff
                    BluetoothAdapter.STATE_ON -> bluetoothState.value = BluetoothState.poweredOn
                }
            }

            BluetoothDevice.ACTION_BOND_STATE_CHANGED -> {
                val currentState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1)
                val previousState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, -1)
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

                logger.i("Device bond state:", bluetoothDeviceConstantsToString(previousState),
                        "to", bluetoothDeviceConstantsToString(currentState), "for",
                        if (device == null) "(no device)" else device.name + "//" + device.address)
            }

            BluetoothDevice.ACTION_FOUND -> {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                val rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MAX_VALUE)

                logger.i("Found device", device.name, "//", device.address)

                if (device != null) {
                    deviceDiscoveredCallback?.invoke(device)
                }

                if (rssi != Short.MAX_VALUE) {
                    _rssiUpdated.onNext(_rssiUpdated.value.plus(Pair(device, rssi)))
                }
            }

            BluetoothAdapter.ACTION_DISCOVERY_STARTED -> scanChangeCallback?.invoke(true)

            BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> scanChangeCallback?.invoke(false)

            BluetoothDevice.ACTION_ACL_DISCONNECTED -> {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                if (device != null) {
                    deviceDisconnected.onNext(device)
                }
            }
        }
    }

    fun bluetoothAdapterConstantsToString(constant: Int): String {
        when (constant) {
            BluetoothAdapter.STATE_OFF -> return "STATE_OFF"
            BluetoothAdapter.STATE_TURNING_ON -> return "STATE_TURNING_ON"
            BluetoothAdapter.STATE_ON -> return "STATE_ON"
            BluetoothAdapter.STATE_TURNING_OFF -> return "STATE_TURNING_OFF"
            BluetoothAdapter.SCAN_MODE_NONE -> return "SCAN_MODE_NONE"
            BluetoothAdapter.SCAN_MODE_CONNECTABLE -> return "SCAN_MODE_CONNECTABLE"
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE -> return "SCAN_MODE_CONNECTABLE_DISCOVERABLE"
            BluetoothAdapter.STATE_DISCONNECTED -> return "STATE_DISCONNECTED"
            BluetoothAdapter.STATE_CONNECTING -> return "STATE_CONNECTING"
            BluetoothAdapter.STATE_CONNECTED -> return "STATE_CONNECTED"
            BluetoothAdapter.STATE_DISCONNECTING -> return "STATE_DISCONNECTING"
            else -> return "Unknown constant: $constant"
        }
    }

    fun bluetoothDeviceConstantsToString(constant: Int): String {
        when (constant) {
            BluetoothDevice.BOND_BONDING -> return "BOND_BONDING"
            BluetoothDevice.BOND_BONDED -> return "BOND_BONDED"
            BluetoothDevice.BOND_NONE -> return "BOND_NONE"
            BluetoothDevice.DEVICE_TYPE_CLASSIC -> return "DEVICE_TYPE_CLASSIC"
            BluetoothDevice.DEVICE_TYPE_DUAL -> return "DEVICE_TYPE_DUAL"
            BluetoothDevice.DEVICE_TYPE_LE -> return "DEVICE_TYPE_LE"
            BluetoothDevice.DEVICE_TYPE_UNKNOWN -> return "DEVICE_TYPE_UNKNOWN"
            BluetoothDevice.ERROR -> return "ERROR"
            else -> return "Unknown constant: $constant"
        }
    }
}