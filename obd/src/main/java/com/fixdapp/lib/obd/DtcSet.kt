package com.fixdapp.lib.obd


/**
 * Created by Matthew on 5/21/2015.
 *
 * A collection of 0 or more [DtcCode]s plus the Check Engine Light state
 */
data class DtcSet(val dtcs: Set<DtcCode>, val isCelOn: Boolean): Set<DtcCode> by dtcs {

    val hasDtcs get() = !isEmpty()

}
