package com.fixdapp.lib.obd

/**
 * Created by  Charles Julian Knight, julian@fixdapp.com  on 4/4/16.
 */
data class DtcCode(val code: String) {

    init {
        if (!isValidCode(code)) throw IllegalArgumentException("Invalid DTC code: " + code)
    }

    companion object {
        fun isValidCode(code: String): Boolean {
            return code.matches("^[PCBU][0-3][0-9A-F]{3}$".toRegex())
        }
    }
}
