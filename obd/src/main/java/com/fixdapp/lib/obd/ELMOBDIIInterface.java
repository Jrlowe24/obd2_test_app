package com.fixdapp.lib.obd;

import android.util.Pair;

import com.fixdapp.android.logger.LogManager;
import com.fixdapp.android.logger.Logger;
import com.fixdapp.lib.ByteArray;
import com.fixdapp.lib.obd.exceptions.InvalidResponseException;
import com.fixdapp.lib.obd.exceptions.VehicleNotConnectedException;
import com.fixdapp.lib.sensor.DataInterface;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okio.Buffer;
import okio.ByteString;

/**
 * Created by charles on 12/9/15.
 * <p>
 * Describes how to communicate the OBDII protocol with an ELM-compatible device.
 * <p>
 * The ELM protocol uses a serial interface with AT commands for configuration and otherwise sending
 * messages as hex strings with carriage return line breaks. See the datasheet for more details
 * <p>
 * With headers enabled, the ELM sends fairly similar responses across all protocols:
 * <p>
 * 11-bit CAN:
 * [7E8] [06] [41 00] BE 1B 30 13
 * [7E9] [06] [41 00] 88 18 00 10
 * [7EA] [06] [41 00] 80 08 00 10
 * <p>
 * 29-bit CAN:
 * [18 DA F1 10] [06] [41 00] BE 1B 30 13
 * [18 DA F1 18] [06] [41 00] 88 18 00 10
 * [18 DA F1 28] [06] [41 00] 80 08 00 10
 * <p>
 * J1850 VPW:
 * [48 6B 10] [41 00] BE 1B 30 13 [EA]
 * [48 6B 18] [41 00] 88 18 00 10 [54]
 * [48 6B 28] [41 00] 80 08 00 10 [06]
 * <p>
 * J1850 PWM:
 * [41 6B 10] [41 00] BE 1B 30 13 [80]
 * [41 6B 18] [41 00] 88 18 00 10 [3E]
 * [41 6B 28] [41 00] 80 08 00 10 [6C]
 * <p>
 * ISO 9141-2:
 * [48 6B 10] [41 00] BE 1B 30 13 [20]
 * [48 6B 18] [41 00] 88 18 00 10 [BC]
 * [48 6B 28] [41 00] 80 08 00 10 [B4]
 * <p>
 * KWP (ISO 14230-4):
 * [86 F1 10] [41 00] BE 1B 30 13 [E4]
 * [86 F1 18] [41 00] 88 18 00 10 [80]
 * [86 F1 28] [41 00] 80 08 00 10 [78]
 * <p>
 * In all cases, three modules, 10, 18, and 28, are responding back. For CAN protocols, after the
 * address is a count of the bytes left in the message (0x06). Then all protocols follow with the
 * response mode (which is the request mode + 0x40) and the requested PID (0x00 in this case).
 * Next is the data (4 bytes for a mode 00 PID). Finally, non-CAN responses end with a protocol-specific
 * byte (CRC or the like).
 * <p>
 * For multiline responses:
 * <p>
 * CAN11
 * [7E8] [10] [0E] [43] [06] 01 00 02 00
 * [7E8] [21]                03 00 43 00 82 00 C1
 * [7E8] [22]                00 00 00 00 00 00 00
 * [7E9]      [04] [43] 01 01 01
 * <p>
 * CAN29
 * [18 DA F1 10] [10] [0E] [43] [06] 01 00 02 00
 * [18 DA F1 10] [21]                03 00 43 00 82 00 C1
 * [18 DA F1 10] [22]                00 00 00 00 00 00 00
 * [18 DA F1 18]      [04] [43] 01 01 01
 * <p>
 * The above shows the result of a multiline response from one module (10) and a single line response
 * from another (18).
 * For CAN, multiline responses contain a PCI byte after the address (10, 21, 22). The first nib
 * is 1 for the first message, and 2 for consecutive messages. The second nib is the index number of
 * the message. Then there's a data count (0x0E = 14 in this case) that says there are 14 bytes of information
 * (including mode). After the header, CAN responses include a count of data items to follow (06). It
 * does the same for other multiline responses as well:
 * <p>
 * Multi-line CAN (VIN)
 * [18 DA F1 10] [10] [14] [49 02] [01] 31 47 31
 * [18 DA F1 10] [21]                   4A 43 35 34 34 34 52
 * [18 DA F1 10] [22]                   37 32 35 32 33 36 37
 * <p>
 * Meanwhile, the non-CAN response does not include any extra information. It's just a list of DTCs, padded with 0x00.
 * Also super-frustratingly, non-CAN responses include line numbers after the header for all multi-line
 * responses EXCEPT {@link PID#GET_DTCS}:
 * <p>
 * Non-CAN (VPW) - GET_DTCS:
 * [48 6B 10] [43] 01 00 02 00 03 00 [05]
 * [48 6B 10] [43] 43 00 82 00 C1 00 [57]
 * [48 6B 18] [43] 01 01 00 00 00 00 [5E]
 * <p>
 * Non-CAN (VPW) - GET_VIN:
 * [48 6B 10] [49 02] [01] 00 00 00 31 [69]
 * [48 6B 10] [49 02] [02] 47 31 4A 43 [5B]
 * [48 6B 10] [49 02] [03] 35 34 34 34 [AA]
 * [48 6B 10] [49 02] [04] 52 37 32 35 [AF]
 * [48 6B 10] [49 02] [05] 32 33 36 37 [0A]
 */
public class ELMOBDIIInterface extends OBDIIInterface {

    private static final Charset ASCII = Charset.forName("US-ASCII");

    private DataInterface dataInterface;
    private Logger logger;

    /**
     * Set up the device so that it's ready for us to check for/pull codes/etc
     */
    public ELMOBDIIInterface(DataInterface dataInterface, LogManager logManager) throws VehicleNotConnectedException {
        super();
        this.dataInterface = dataInterface;
        this.logger = logManager.getNewLogger();
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    /**
     * We need to be able to handle both the case of multiple ECUs responding and the ELM's special multi-line
     * responses.
     */
    @Override
    public synchronized List<PIDResponse> sendMessage(Mode mode, Byte pid, @Nullable String id,
                                                      @Nullable byte[] outgoingExtraData)
            throws VehicleNotConnectedException, InvalidResponseException {
        try {
            if (id != null) {
                id = id.toUpperCase().replaceAll("[^A-F0-9]", "");
                logger.tag("ELM").d("Limiting request to module", id);
                sendCommand("ATCRA" + id);
            }
            int outgoingMessageLength;
            switch (mode) {
                case CURRENT_DATA:
                case VEHICLE_INFO:
                    outgoingMessageLength = 2; //mode, pid
                    break;
                case FREEZE_DATA:
                    outgoingMessageLength = 3; //mode, pid, frame index
                    break;
                case STORED_DTCS:
                case PENDING_DTCS:
                case PERMANENT_DTCS:
                case CLEAR_STORED_DTCS:
                    outgoingMessageLength = 1; //mode
                    break;
                default:
                    outgoingMessageLength = 2 + (outgoingExtraData == null ? 0 : outgoingExtraData.length);
            }

            byte[] outgoingMessage = new byte[outgoingMessageLength];
            outgoingMessage[0] = mode.getCode();
            if (outgoingMessageLength > 1) {
                outgoingMessage[1] = pid;
            }
            if (outgoingExtraData != null) {
                if (outgoingMessageLength - 2 != outgoingExtraData.length) {
                    throw new IllegalArgumentException("Wrong number of data bytes given." +
                            " Expected " + (outgoingMessageLength - 2) + ", got " + outgoingExtraData.length);
                }
                System.arraycopy(outgoingExtraData, 0, outgoingMessage, 2, outgoingExtraData.length);
            }

            Protocol currentProtocol = getProtocol();

            String raw = sendCommand(outgoingMessage);
            if (raw.contains("NO DATA")) {
                return new ArrayList<>(0); //no responses
            }
            int expectedHeaderLength = outgoingMessageLength;

            //split into lines and filter out duplicates
            String[] rawArray = raw.split("\\r");
            List<String> rawResults = new ArrayList<>(rawArray.length);
            for (String rawResult : rawArray) {
                if (!rawResults.contains(rawResult)) {
                    rawResults.add(rawResult);
                }
            }

            if (rawResults.size() == 1) { //if only one line, then a single response from a single module
                if (rawResults.get(0).matches("[A-F0-9\\s]+")) {
                    Pair<String, byte[]> r = separateId(rawResults.get(0), currentProtocol);
                    if (id == null || id.equals(r.first)) {

                        PIDResponse response = fromSingleLine(mode, PID.get(mode, pid), currentProtocol, r.first, expectedHeaderLength, r.second);
                        if (response.isValid()) {
                            return Collections.singletonList(response);
                        }
                    }
                }
                //no valid responses
                return new ArrayList<>(0);
            }

            // for multiline responses or responses from multiple modules
            Map<String, List<byte[]>> groupedResponses = new HashMap<>(rawResults.size()); //group responses by sender
            for (String rawResult : rawResults) {
                if (rawResult.matches("[A-F0-9\\s]+")) { //response should be a hex string
                    Pair<String, byte[]> r = separateId(rawResult, currentProtocol);
                    if (id == null || id.equals(r.first)) {
                        List<byte[]> responseLines = groupedResponses.get(r.first);
                        if (responseLines == null) {
                            responseLines = new ArrayList<>();
                        }
                        responseLines.add(r.second);
                        groupedResponses.put(r.first, responseLines);
                    }
                }
            }
            List<PIDResponse> combinedResponses = new ArrayList<>(groupedResponses.size());
            for (Map.Entry<String, List<byte[]>> responseLines : groupedResponses.entrySet()) {
                //once responses are grouped by sender, then for each sender
                PIDResponse response;
                if (responseLines.getValue().size() == 1) {
                    //if the sender just sent one message, just add the one

                    response = fromSingleLine(mode, PID.get(mode, pid), currentProtocol,
                            responseLines.getKey(), expectedHeaderLength, responseLines.getValue().get(0));
                } else {
                    //otherwise it's a multiline response, so reduce to one PIDResponse
                    response = fromMultiline(mode, PID.get(mode, pid), currentProtocol,
                            responseLines.getKey(), expectedHeaderLength, responseLines.getValue());
                }
                if (response.isValid()) {
                    combinedResponses.add(response);
                } else {
                    logger.w("Ignoring invalid response", response);
                }
            }
            // sort response groups by address so they always come back in the same order
            Collections.sort(combinedResponses, new Comparator<PIDResponse>() {
                @Override
                public int compare(PIDResponse lhs, PIDResponse rhs) {
                    return lhs.getResponderId().compareTo(rhs.getResponderId());
                }
            });
            return combinedResponses;
        } catch (IOException e) {
            throw new VehicleNotConnectedException(e);
        } finally {
            if (id != null) {
                try {
                    logger.tag("ELM").d("re-enabling responses from all modules");
                    sendCommand("ATAR"); //return to receiving all messages
                } catch (IOException e) {
                    //probably already caught
                }
            }
        }
    }

    /**
     * We need to be able to handle both the case of multiple ECUs responding and the ELM's special multi-line
     * responses.
     */
    @Override
    public synchronized List<PIDResponse> sendMessageHex(String hexMode, String hexPid, @Nullable String id, @Nullable byte[] outgoingExtraData)
            throws VehicleNotConnectedException, InvalidResponseException {
        try {
            if (id != null) {
                id = id.toUpperCase().replaceAll("[^A-F0-9]", "");
                logger.tag("ELM").d("Limiting request to module", id);
                sendCommand("ATCRA" + id);
            }

            Protocol currentProtocol = getProtocol();
            hexMode = hexMode.trim().length() % 2 == 0 ? hexMode.trim() : "0" + hexMode.trim();
            hexPid = hexPid.trim().length() % 2 == 0 ? hexPid.trim() : "0" + hexPid.trim();

            String message = hexMode + hexPid;
            String raw = sendCommand(message.toUpperCase());

            if (raw.contains("NO DATA")) {
                return new ArrayList<>(0); //no responses
            }
            int expectedHeaderLength = (hexMode.length() + hexPid.length()) / 2;
            logger.d("expectedHeaderLength: " + expectedHeaderLength);

            //split into lines and filter out duplicates
            String[] rawArray = raw.split("\\r");
            List<String> rawResults = new ArrayList<>(rawArray.length);
            for (String rawResult : rawArray) {
                if (!rawResults.contains(rawResult)) {
                    rawResults.add(rawResult);
                }
            }
            if (rawResults.size() == 1) { //if only one line, then a single response from a single module
                if (rawResults.get(0).matches("[A-F0-9\\s]+")) {
                    String first_result = rawResults.get(0);
                    Pair<String, byte[]> r = separateId(first_result, currentProtocol);

                    if (id == null || id.equals(r.first)) {

                        String[] strResponse = first_result.split(" ");
                        String hexResponseMode = "";
                        if (strResponse.length > (hexMode.length() % 2) + 1) {
                            hexResponseMode = strResponse[(hexMode.length() % 2) + 2];
                        }
                        logger.d("r.first: " + r.first + ", r.second: " + Arrays.toString(r.second)
                                + ", hexResponseMode: " + hexResponseMode);

                        PIDResponse response = fromSingleLine(hexMode, hexPid, currentProtocol, r.first, expectedHeaderLength, r.second);
//                        logger.tag("****").d("response: "+ response.getRawResponses()+ ", getRequestedMode: "+ response.getRequestedMode());
                        if (response.isValid()) {
                            return Collections.singletonList(response);
                        }
                    }
                }
                //no valid responses
                return new ArrayList<>(0);
            }

            // for multiline responses or responses from multiple modules
            Map<String, List<byte[]>> groupedResponses = new HashMap<>(rawResults.size()); //group responses by sender
            for (String rawResult : rawResults) {
                if (rawResult.matches("[A-F0-9\\s]+")) { //response should be a hex string
                    Pair<String, byte[]> r = separateId(rawResult, currentProtocol);
                    if (id == null || id.equals(r.first)) {
                        List<byte[]> responseLines = groupedResponses.get(r.first);
                        if (responseLines == null) {
                            responseLines = new ArrayList<>();
                        }
                        responseLines.add(r.second);
                        groupedResponses.put(r.first, responseLines);
                    }
                }
            }
            List<PIDResponse> combinedResponses = new ArrayList<>(groupedResponses.size());
            for (Map.Entry<String, List<byte[]>> responseLines : groupedResponses.entrySet()) {
                //once responses are grouped by sender, then for each sender
                PIDResponse response;

                if (responseLines.getValue().size() == 1) {
                    //if the sender just sent one message, just add the one

                    response = fromSingleLine(hexMode, hexPid, currentProtocol,
                            responseLines.getKey(), expectedHeaderLength, responseLines.getValue().get(0));
                } else {
                    //otherwise it's a multiline response, so reduce to one PIDResponse
                    response = fromMultiline(hexMode, hexPid, currentProtocol,
                            responseLines.getKey(), expectedHeaderLength, responseLines.getValue());
                }
                if (response.isValid()) {
                    combinedResponses.add(response);
                } else {
                    logger.w("Ignoring invalid response", response);
                }
//                logger.tag("***").d("response: "+ response.getRawResponses()+ ", getRequestedMode: "+ response.getRequestedMode());
            }
            // sort response groups by address so they always come back in the same order
            Collections.sort(combinedResponses, new Comparator<PIDResponse>() {
                @Override
                public int compare(PIDResponse lhs, PIDResponse rhs) {
                    return lhs.getResponderId().compareTo(rhs.getResponderId());
                }
            });
            return combinedResponses;
        } catch (IOException e) {
            throw new VehicleNotConnectedException(e);
        } finally {
            if (id != null) {
                try {
                    logger.tag("ELM").d("re-enabling responses from all modules");
                    sendCommand("ATAR"); //return to receiving all messages
                } catch (IOException e) {
                    //probably already caught
                }
            }
        }
    }

    /**
     * We need to be able to handle both the case of multiple ECUs responding and the ELM's special multi-line
     * responses.
     */
    @Override
    public synchronized StringBuffer readBroadcastData(String id)
            throws VehicleNotConnectedException, InvalidResponseException {
        try {

            StringBuffer braodcastResponse = new StringBuffer();
            if (id != null) {
                id = id.toUpperCase().replaceAll("[^A-F0-9]", "");
                logger.tag("ELM").d("Limiting request to module", id);
                sendCommand("ATCRA" + id);

                sendCommand("0100");
                logger.tag("ELM").d("Reading Broadcast Data on", id);
//                braodcastResponse.append(sendCommand("AT MA"));
                braodcastResponse.append(receiveBroadcasts("ATMA"));
            }

            return braodcastResponse;
        } catch (IOException e) {
            throw new VehicleNotConnectedException(e);
        } finally {
            if (id != null) {
                try {
                    logger.tag("ELM").d("re-enabling responses from all modules");
                    sendCommand("ATAR"); //return to receiving all messages
                } catch (IOException e) {
                    //probably already caught
                }
            }
        }
    }

    @Override
    public void cleanUp() throws IOException {
        if (cachedProtocol != null && cachedProtocol.isISO()) {
            sendCommand("ATWS"); // perform a soft reset to disable keep-alive
        }
        super.cleanUp();
    }

    public void changeRequestAddress(String hex_address) throws IOException {
        // change Request Address (header)
        sendCommand("ATSH " + hex_address);
    }

    @Override
    public void close() throws IOException {
        dataInterface.close();
    }

    private Protocol cachedProtocol;

    @Override
    public Protocol getProtocol() throws VehicleNotConnectedException, InvalidResponseException {
        if (cachedProtocol != null) {
            return cachedProtocol;
        }
        cachedProtocol = getProtocolFromVehicle();
        return cachedProtocol;
    }

    private Protocol getProtocolFromVehicle() throws VehicleNotConnectedException, InvalidResponseException {
    /*
        1 - SAE J1850 PWM (41.6 kbaud)
        2 - SAE J1850 VPW (10.4 kbaud)
        3 - ISO 9141-2 (5 baud init, 10.4 kbaud)
        4 - ISO 14230-4 KWP (5 baud init, 10.4 kbaud)
        5 - ISO 14230-4 KWP (fast init, 10.4 kbaud)
        6 - ISO 15765-4 CAN (11 bit ID, 500 kbaud)
        7 - ISO 15765-4 CAN (29 bit ID, 500 kbaud)
        8 - ISO 15765-4 CAN (11 bit ID, 250 kbaud)
        9 - ISO 15765-4 CAN (29 bit ID, 250 kbaud)
        A - SAE J1939 CAN (29 bit ID, 250* kbaud)
     */
        try {
            //in automatic mode, will return AX instead of X
            int n = ByteArray.hexToByte(sendCommand("ATDPN").trim()) & 0x0F;
            switch (n) {
                case 1:
                    return Protocol.J1850_PWM;
                case 2:
                    return Protocol.J1850_VPW;
                case 3:
                    return Protocol.ISO_9141_2;
                case 4:
                    return Protocol.ISO_14230_4_KWP;
                case 5:
                    return Protocol.ISO_14230_4_KWP_FAST;
                case 6:
                    return Protocol.CAN_11_500;
                case 7:
                    return Protocol.CAN_29_500;
                case 8:
                    return Protocol.CAN_11_250;
                case 9:
                    return Protocol.CAN_29_250;
                default:
                    throw new IllegalArgumentException("Invalid protocol: " + n);
            }
        } catch (IOException e) {
            throw new VehicleNotConnectedException(e);
        }
    }

    public void setProtocol(int n) throws VehicleNotConnectedException {
        int hex_Proto = n;
        Protocol newProto = null;
        try {
            switch (n) {
                case 1:
                    newProto = Protocol.J1850_PWM;
                    return;
                case 2:
                    newProto = Protocol.J1850_VPW;
                    return;
                case 3:
                    newProto = Protocol.ISO_9141_2;
                    return;
                case 4:
                    newProto = Protocol.ISO_14230_4_KWP;
                    return;
                case 5:
                    newProto = Protocol.ISO_14230_4_KWP_FAST;
                    return;
                case 6:
                    newProto = Protocol.CAN_11_500;
                    return;
                case 7:
                    newProto = Protocol.CAN_29_500;
                    return;
                case 8:
                    newProto = Protocol.CAN_11_250;
                    return;
                case 9:
                    newProto = Protocol.CAN_29_250;
                    return;

            }
            if (newProto == null) {
                throw new IllegalArgumentException("Invalid protocol: " + n);
            }
            cachedProtocol = newProto;
            sendCommand("ATSP " + Integer.toHexString(hex_Proto));

        } catch (Exception e) {
            throw new VehicleNotConnectedException(e);
        }

    }


    public void setProtocolAutoDetect() throws VehicleNotConnectedException, InvalidResponseException {
        try {
            sendCommand("ATSP0");
        } catch (IOException e) {
            throw new VehicleNotConnectedException(e);
        }
    }

    public Pair<Long, Double> getBatteryValue() throws
            VehicleNotConnectedException, InvalidResponseException {
        try {
            return new Pair<>(System.nanoTime(), Double.parseDouble(sendCommand("ATRV").replaceAll("V", "").trim()));
        } catch (Exception e) {
            throw new VehicleNotConnectedException(e);
        }
    }

    protected String sendCommand(byte[] request) throws IOException {
        return sendCommand(ByteArray.bytesToHex(request));
    }

    private long lastISOResponse = 0;
    private static final long MIN_TIME_BETWEEN_ISO_KEEP_ALIVES = 2500;

    public String sendCommand(String request) throws IOException {
        String result = _sendCommand(request);
        /*
            The ELM has built-in support for sending messages at regular intervals for keeping the channel
            open on the protocols that require an init. However, if we send a whole lot of commands in a
            row, it won't interrupt us to send keep-alive commands. Normally this would be fine, since
            by talking to the vehicle we should be keeping the connection alive. However, requests where
            no module responds don't count towards keeping the connection alive. So here we implement
            our own hacky keep-alive, where if it has been a while since we got a response from the vehicle
            that actually has data, we send a command that should actually have a response.
         */
        if (cachedProtocol != null && cachedProtocol.isISO()) {
            if (result.contains("NO DATA")) {
                if ((System.currentTimeMillis() - lastISOResponse) > MIN_TIME_BETWEEN_ISO_KEEP_ALIVES) {
                    _sendCommand("0100");
                    lastISOResponse = System.currentTimeMillis();
                }
            } else {
                lastISOResponse = System.currentTimeMillis();
            }
        }
        return result;
    }

    private synchronized String _sendCommand(String request) throws IOException {
        //all commands must end with a carriage return
        if (!request.endsWith("\r")) {
            request = request + "\r";
        }
        logger.tag("ELM").i("SEND:", request.replace("\r", "\\r")); //encode \r so logger doesn't freak out

        int retryCount = 3;
        while (true) {

            // Write the request to the device's output stream
            ByteString result = dataInterface.write(ByteString.encodeString(request, ASCII), (byte) '>');
            // There is a known (hardware) bug in the ELM327's PIC microcontroller's serial subsystem
            // that *sometimes* causes a stray null byte to be transmitted.  Drop those.
            // See pg 9 of ELM datasheet
            Buffer buf = new Buffer();
            for (byte b : result.toByteArray()) {
                if (b != 0) {
                    buf.writeByte(b);
                }
            }
            String response = buf.readString(ASCII).replace(">", "");
            logger.i("RECV:", response.replace("\r", "\\r"));
            if (response.contains("UNABLE TO CONNECT")) {   // If there's nobody home, panic
                // We try again, unless we don't then we throw an exception
                if (retryCount-- == 0) {
                    throw new IOException("Problem communicating with vehicle: " + response);
                }
                logger.w("Failed to connect. retrying (", retryCount, ")...");
            } else {
                return response;
            }
        }
    }

    private synchronized String receiveBroadcasts(String request) throws IOException {
        //all commands must end with a carriage return
        if (!request.endsWith("\r")) {
            request = request + "\r";
        }
        logger.tag("ELM").i("SEND:", request.replace("\r", "\\r")); //encode \r so logger doesn't freak out

        int retryCount = 3;
        long lStartTime = System.nanoTime();
        long lEndTime = System.nanoTime();

        String response = "";

        while ((lEndTime - lStartTime) / 1000000000 < 10) {
            System.out.println("Elapsed time in seconds: " + (lEndTime - lStartTime) / 1000000000);

            // Write the request to the device's output stream
            ByteString result = dataInterface.write(ByteString.encodeString(request, ASCII), (byte) '>');
            // There is a known (hardware) bug in the ELM327's PIC microcontroller's serial subsystem
            // that *sometimes* causes a stray null byte to be transmitted.  Drop those.
            // See pg 9 of ELM datasheet
            Buffer buf = new Buffer();
            for (byte b : result.toByteArray()) {
                if (b != 0) {
                    buf.writeByte(b);
                }
            }
            response = buf.readString(ASCII).replace(">", "");
            logger.i("RECV:", response.replace("\r", "\\r"));
            if (response.contains("UNABLE TO CONNECT")) {   // If there's nobody home, panic
                // We try again, unless we don't then we throw an exception
                if (retryCount-- == 0) {
                    throw new IOException("Problem communicating with vehicle: " + response);
                }
                logger.w("Failed to connect. retrying (", retryCount, ")...");
            } else {
                return response;
            }
        }
        return response;
    }

    @Nullable
    @Override
    public String getVin() throws VehicleNotConnectedException, InvalidResponseException {
        String vin = super.getVin();
        if (vin == null && cachedProtocol.isISO()) {
            // on some old vehicles, asking for the VIN seems to corrupt the connection and give
            // NO DATA over and over. so do a reset just to be safe
            getLogger().i("vin was null on ISO protocol. Trying a reset");
            reset();
            cachedProtocol = getProtocolFromVehicle();
        }
        return vin;
    }

    @Override
    public void reset() throws VehicleNotConnectedException, InvalidResponseException {
        super.reset();
        cachedProtocol = null;
        try {
            String cmd = sendCommand("ATZ");
            int attempts = 0;
            while (!cmd.contains("ELM") && !cmd.contains("EST") && attempts++ < 10) {
                cmd = sendCommand("");
            }
            // Reset the used protocol to none (auto-discover)
            setProtocolAutoDetect();
            // Enable extra bus headers. this not only tells us who is responding, but auto-combines multi-line responses
            sendCommand("ATH1");
            // Turn off linefeeds
            sendCommand("ATL0");
            //turn off echo back
            sendCommand("ATE0");
            //disable spaces between response bytes
            if (!BuildConfig.DEBUG) {
                sendCommand("ATS0");
            }

            // Send a command so we can detect the protocol now instead of later
            sendCommand("0100");
        } catch (IOException e) {
            throw new VehicleNotConnectedException(e);
        }
    }

    @Override
    public void checkConnected() throws IOException {
        sendCommand("ATI");
        sendCommand("0100");
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Create a PIDResponse from a single line of hex data from an ELM-compatible device
     *
     * @param mode         the requested mode
     * @param pid          the requested PID
     * @param protocol     the protocol used
     * @param id           the address of the responding module as hex String
     * @param headerLength the number of header (mode, pid, etc) bytes to expect
     * @param raw          the raw data of the response except for the id
     * @throws InvalidResponseException if the line doesn't follow the expected format
     */
    private static PIDResponse fromSingleLine(Mode mode, PID pid, Protocol protocol, String id,
                                              int headerLength, byte[] raw) throws InvalidResponseException {
        try {
            ByteBuffer bb = ByteBuffer.wrap(raw);
            int dataSize = 0;
            dataSize = bb.get(); //the first byte is bytes remaining

            byte[] headers = new byte[headerLength];
            bb.get(headers, 0, headerLength);
            if (!protocol.isCAN()) {
                dataSize = bb.remaining() - 1; //for the header
            } else {
                dataSize -= headerLength; // minus the headers we just copied
            }
            byte[] data = new byte[dataSize];
            bb.get(data, 0, dataSize);
            List<Byte> checksum = protocol.isCAN() ? null : Collections.singletonList(bb.get(dataSize));
            return new PIDResponse(mode.getHexCode(), pid.getHexCode(), protocol, false, id, headers, data, checksum, Collections.singletonList(raw));
        } catch (Exception e) {
            throw new InvalidResponseException(e);
        }
    }

    /**
     * Create a PIDResponse from a single line of hex data from an ELM-compatible device
     *
     * @param mode         the requested mode
     * @param pid          the requested PID
     * @param protocol     the protocol used
     * @param id           the address of the responding module as hex String
     * @param headerLength the number of header (mode, pid, etc) bytes to expect
     * @param raw          the raw data of the response except for the id
     * @throws InvalidResponseException if the line doesn't follow the expected format
     */
    private static PIDResponse fromSingleLine(String mode, String pid, Protocol
            protocol, String id, int headerLength, byte[] raw) throws InvalidResponseException {
        try {
            ByteBuffer bb = ByteBuffer.wrap(raw);
            int dataSize = 0;
            if (protocol.isCAN()) {
                dataSize = bb.get(); //the first byte is bytes remaining
            }
            byte[] headers = new byte[headerLength];
            bb.get(headers, 0, headerLength);
            if (!protocol.isCAN()) {
                dataSize = bb.remaining() - 1; //for the header
            } else {
                dataSize -= headerLength; // minus the headers we just copied
            }
            byte[] data = new byte[dataSize];
            bb.get(data, 0, dataSize);

            List<Byte> checksum = protocol.isCAN() ? null : Collections.singletonList(bb.get(dataSize));
            return new PIDResponse(mode, pid, protocol, false, id, headers, data, checksum, Collections.singletonList(raw));
        } catch (Exception e) {
            throw new InvalidResponseException(e);
        }
    }

    /**
     * Create a PIDResponse from multiple lines of hex data from the same module
     *
     * @param mode         the requested mode
     * @param pid          the requested PID
     * @param protocol     the protocol used
     * @param id           the address of the responding module as hex String
     * @param headerLength he number of header (mode, pid, etc) bytes to expect
     * @param lines        the raw data of the responses with the ids removed
     * @throws InvalidResponseException if the line doesn't follow the expected format
     */
    private static PIDResponse fromMultiline(Mode mode, PID pid, Protocol protocol, String id,
                                             int headerLength, List<byte[]> lines) throws InvalidResponseException {
        try {
            //for multiline-CAN, after the header is one byte that says how many elements follow
//            int actualHeaderLength = protocol.isCAN() ? headerLength+1 : headerLength;
            byte[] headers = new byte[headerLength];
            ByteBuffer bb = null;
            if (protocol.isCAN()) {
                //CAN
                byte[][] orderedBytes = new byte[lines.size()][0]; //lines may not be in order, so we need to first order the data
                for (byte[] line : lines) {
                    int order = (line[0] & 0x0F); // the first byte is the PCI byte
                    int firstDataByte = 1; // by default, skip the order byte
                    if (order == 0) { // the first line contains the bytecount and headers (mode, pid, etc)
                        int totalBytes = line[firstDataByte++] & 0xFF;
                        bb = ByteBuffer.allocate(totalBytes);
                        System.arraycopy(line, firstDataByte, headers, 0, headerLength);
                        firstDataByte += headerLength; // now start reading data after headers
                    }
                    byte[] lineData = new byte[line.length - firstDataByte];
                    System.arraycopy(line, firstDataByte, lineData, 0, lineData.length);
                    orderedBytes[order] = lineData;
                }
                if (bb == null) {
                    //never got first line!
                    throw new InvalidResponseException("no first line in multiline can response");
                }
                for (byte[] lineData : orderedBytes) { //now, for all bytes in the right order
                    if (lineData == null) {
                        throw new InvalidResponseException("missing line in response");
                    }
                    // add them to the buffer
                    if (lineData.length > bb.remaining()) {
                        bb.put(lineData, 0, bb.remaining());
                    } else {
                        bb.put(lineData);
                    }
                }
            } else {
                //non-CAN
                bb = ByteBuffer.allocate(6 * lines.size()); //we don't have byte counts, but they should always be 6 bytes per line
                System.arraycopy(lines.get(0), 0, headers, 0, headerLength); //get the headers from the beginning of the first line
                if (pid == PID.GET_DTCS) { // multiline PIDs except GET_DTCS have a line number after header
                    for (byte[] line : lines) {
                        bb.put(line, headerLength, line.length - headerLength - 1);
                    }
                } else {
                    byte[][] orderedLines = new byte[lines.size()][0];
                    for (byte[] line : lines) {
                        int lineNum = line[headerLength] - 1; //after the header is a byte for line number
                        orderedLines[lineNum] = line;
                    }
                    for (byte[] line : orderedLines) {
                        bb.put(line, headerLength + 1, line.length - headerLength - 2); //add all non-header, non-footer data to buffer
                    }
                }
            }
            bb.flip(); //set pointer back to zero and start reading what was placed in it
            byte[] data = new byte[bb.remaining()];
            bb.get(data, 0, data.length);
            List<Byte> checksums = null;
            if (!protocol.isCAN()) {
                checksums = new ArrayList<>(lines.size());
                for (byte[] line : lines) {
                    checksums.add(line[line.length - 1]);
                }
            }
            return new PIDResponse(mode.getHexCode(), pid.getHexCode(), protocol, true, id, headers, data, checksums, lines);
        } catch (Exception e) {
            throw new InvalidResponseException(e);
        }
    }

    /**
     * Create a PIDResponse from multiple lines of hex data from the same module
     *
     * @param mode         the requested mode
     * @param pid          the requested PID
     * @param protocol     the protocol used
     * @param id           the address of the responding module as hex String
     * @param headerLength he number of header (mode, pid, etc) bytes to expect
     * @param lines        the raw data of the responses with the ids removed
     * @throws InvalidResponseException if the line doesn't follow the expected format
     */
    private static PIDResponse fromMultiline(String mode, String pid, Protocol protocol, String
            id, int headerLength, List<byte[]> lines) throws InvalidResponseException {
        try {
            //for multiline-CAN, after the header is one byte that says how many elements follow
//            int actualHeaderLength = protocol.isCAN() ? headerLength+1 : headerLength;
            byte[] headers = new byte[headerLength];
            ByteBuffer bb = null;
            if (protocol.isCAN()) {
                //CAN
                byte[][] orderedBytes = new byte[lines.size()][0]; //lines may not be in order, so we need to first order the data
                for (byte[] line : lines) {
                    int order = (line[0] & 0x0F); // the first byte is the PCI byte
                    int firstDataByte = 1; // by default, skip the order byte
                    if (order == 0) { // the first line contains the bytecount and headers (mode, pid, etc)
                        int totalBytes = line[firstDataByte++] & 0xFF;
                        bb = ByteBuffer.allocate(totalBytes);
                        System.arraycopy(line, firstDataByte, headers, 0, headerLength);
                        firstDataByte += headerLength; // now start reading data after headers
                    }
                    byte[] lineData = new byte[line.length - firstDataByte];
                    System.arraycopy(line, firstDataByte, lineData, 0, lineData.length);
                    orderedBytes[order] = lineData;
                }
                if (bb == null) {
                    //never got first line!
                    throw new InvalidResponseException("no first line in multiline can response");
                }
                for (byte[] lineData : orderedBytes) { //now, for all bytes in the right order
                    if (lineData == null) {
                        throw new InvalidResponseException("missing line in response");
                    }
                    // add them to the buffer
                    if (lineData.length > bb.remaining()) {
                        bb.put(lineData, 0, bb.remaining());
                    } else {
                        bb.put(lineData);
                    }
                }
            }
            bb.flip(); //set pointer back to zero and start reading what was placed in it
            byte[] data = new byte[bb.remaining()];
            bb.get(data, 0, data.length);
            List<Byte> checksums = null;
            if (!protocol.isCAN()) {
                checksums = new ArrayList<>(lines.size());
                for (byte[] line : lines) {
                    checksums.add(line[line.length - 1]);
                }
            }
            return new PIDResponse(mode, pid, protocol, true, id, headers, data, checksums, lines);
        } catch (Exception e) {
            throw new InvalidResponseException(e);
        }
    }

    /**
     * Separate a line into its responder ID and the remaining data
     */
    private static Pair<String, byte[]> separateId(String line, Protocol protocol) throws
            InvalidResponseException {
        try {
            int idSize = 3;
            if (protocol.isCAN()) {
                idSize = protocol.isExtendedCAN() ? 4 : 2;
            }
            byte[] data = ByteArray.hexToBytes(line);
            byte[] id = new byte[idSize];
            byte[] remaining = new byte[data.length - id.length];
            System.arraycopy(data, 0, id, 0, idSize);
            System.arraycopy(data, idSize, remaining, 0, remaining.length);
            return new Pair<>(ByteArray.bytesToHex(id), remaining);
        } catch (Exception e) {
            throw new InvalidResponseException(e);
        }
    }
}
