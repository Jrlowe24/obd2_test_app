package com.fixdapp.lib.obd.exceptions;

/**
 * Created by Matthew on 5/11/2015.
 */
public class VehicleNotConnectedException extends Exception {
    public VehicleNotConnectedException(){
        super();
    }

    public VehicleNotConnectedException(Throwable t){
        super(t);
    }
}
