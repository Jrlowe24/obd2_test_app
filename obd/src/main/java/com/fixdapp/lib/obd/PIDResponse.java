package com.fixdapp.lib.obd;

import android.support.annotation.Nullable;

import com.fixdapp.lib.ByteArray;

import java.util.ArrayList;
import java.util.List;

import okio.Buffer;
import okio.ByteString;

/**
 * Created by charles on 12/9/15.
 * A class for representing the response from an OBD-II request.
 *
 * A PIDResponse is comprised of:
 *  - the Mode and PID requested
 *  - the Protocol used in the transaction
 *  - A hex String representation of the responder's ID
 *  - an array of headers (the responding Mode, PID if applicable, frame index if applicable, etc)
 *  - the actual data with all of this other stuff removed, in the form of a {@link ByteArray}
 */
public class PIDResponse {

    private ByteArray rawData;
    private String requestedMode;
    private String requestedPID;
    private byte[] headers;
    // only set on multiline CAN responses
    private Integer itemCount;
    private Protocol protocol;
    private String responderId;
    private List<Byte> checksums;
    private List<byte[]> rawResponses;

    public PIDResponse(String requestedMode, String requestedPID, Protocol protocol, boolean isMultiline, String responderId, byte[] headers, byte[] data, List<Byte> checksums, List<byte[]> rawResponses){
        this.requestedMode = requestedMode;
        this.requestedPID = requestedPID;
        this.headers = headers;
        this.protocol = protocol;
        this.responderId = responderId;
        if (protocol.isCAN() && isMultiline) {
            itemCount = (int) data[0];
            byte[] subData = new byte[data.length - 1];
            System.arraycopy(data, 1, subData, 0, subData.length);
            rawData = new ByteArray(subData);
        } else {
            itemCount = null;
            rawData = new ByteArray(data);
        }
        this.checksums = checksums;
        this.rawResponses = new ArrayList<>(rawResponses.size());
        int i = 0;
        for(byte[] rawResponse : rawResponses) {
            Buffer b = new Buffer();
            b.write(ByteString.decodeHex(responderId));
            b.write(rawResponse);
            if (checksums != null) {
                b.writeByte(checksums.get(i));
            }
            this.rawResponses.add(b.readByteArray());
            i++;
        }
        rawData = new ByteArray(data);

    }

    /**
     * a response is valid if:
     *  - the returned mode is 0x40 + requested mode
     *  - the pids match
     *  - the response data has at least as many bytes as required
     */
    public boolean isValid() {
        return true;
//        return (getReturnedMode() == (requestedMode.getCode() + 0x40))
//                && (getReturnedPID() == requestedPID.getCode())
//                && (requestedPID.getBytesReturned() < 0 || rawData.size() >= requestedPID.getBytesReturned());
    }

    public ByteArray getData(){
        return rawData;
    }

    public String getRequestedMode(){ return requestedMode; }

    public String getReturnedMode(){
        return bytesToHex(new byte[] {headers[0]});
    }

    public String getRequestedPID(){ return requestedPID; }

    public int getRequestedPIDInt(){
        return (int) Long.parseLong(requestedPID, 16);
    }

    public String getReturnedPID(){
        // for GET_DTCS, there is no returned PID, but there is a header for DTC count
        // for everything else, if it has another header byte, that should be the PID
        return requestedPID;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public String getResponderId(){
        return responderId;
    }

    @Nullable
    public Integer getItemCount() {
        return itemCount;
    }

    public String toString(){
        return responderId +": " + headers +"  "+ByteArray.bytesToHex(rawData.raw());
    }

    public List<Byte> getChecksums() {
        return checksums;
    }

    public List<byte[]> getRawResponses() {
        return rawResponses;
    }

    /**
     * Reduce a set of responses by bitwise-OR-ing all the data fields together
     */
    public static ByteArray reduceByMask(List<PIDResponse> responses){
        if(responses.size() == 1) {
            return responses.get(0).getData();
        }else{
            int bytesReturned = responses.get(0).getData().size();
            byte[] combined = new byte[bytesReturned];
            for (PIDResponse response : responses) {
                if (response.getRequestedMode().equals(responses.get(0).getRequestedMode())) {
                    throw new IllegalArgumentException("All responses must have the same mode");
                }
                if (response.getRequestedPID().equals(responses.get(0).getRequestedPID())) {
                    throw new IllegalArgumentException("All responses must have the the same pid");
                }
                for (int i = 0; i < bytesReturned; i++) {
                    combined[i] |= response.getData().raw()[i];
                }
            }
            return new ByteArray(combined);
        }
    }
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
