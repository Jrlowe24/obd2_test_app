package com.fixdapp.lib.obd;

import android.content.Context;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by matth on 8/19/2015.
 *
 * Make/Country/Year is based off https://github.com/genderquery/vin-decode
 *
 * https://en.wikibooks.org/wiki/Category:Vehicle_Identification_Numbers_(VIN_codes)
 */
public class VinValidator {

    private static final Map<Character, Integer> transliterationKeys;
    private static final int[] weights = {8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2};

    static {
        transliterationKeys = new HashMap<>();
        transliterationKeys.put('A', 1);
        transliterationKeys.put('B', 2);
        transliterationKeys.put('C', 3);
        transliterationKeys.put('D', 4);
        transliterationKeys.put('E', 5);
        transliterationKeys.put('F', 6);
        transliterationKeys.put('G', 7);
        transliterationKeys.put('H', 8);
        transliterationKeys.put('J', 1);
        transliterationKeys.put('K', 2);
        transliterationKeys.put('L', 3);
        transliterationKeys.put('M', 4);
        transliterationKeys.put('N', 5);
        transliterationKeys.put('P', 7);
        transliterationKeys.put('R', 9);
        transliterationKeys.put('S', 2);
        transliterationKeys.put('T', 3);
        transliterationKeys.put('U', 4);
        transliterationKeys.put('V', 5);
        transliterationKeys.put('W', 6);
        transliterationKeys.put('X', 7);
        transliterationKeys.put('Y', 8);
        transliterationKeys.put('Z', 9);

        // Numbers get their own value
        for(int i = 0; i < 10; i++)
        {
            transliterationKeys.put((char)(i + '0'), i);
        }

        // Put unused chars in the map anyway
        transliterationKeys.put('I', 0);
        transliterationKeys.put('O', 0);
        transliterationKeys.put('Q', 0);
    }

    private Map<String, String> makeSet;
    private Map<String, String> regionSet;
    private Map<String, String> countrySet;

    public VinValidator(Context context){
        initialize(context);
    }

    private boolean passesChecksum(String vin){

        int sum = 0;

        for (int i = 0; i < 17; i++) {
            char c = vin.charAt(i);
            int key = transliterationKeys.get(c);
            sum += key * weights[i];
        }

        int remainder = sum % 11;

        char actualCheckDigit = vin.charAt(8);
        char calculatedCheckDigit = (remainder == 10) ? 'X' : (char) (remainder + '0');

        return actualCheckDigit == calculatedCheckDigit;
    }

    public boolean isValidVin(String vin) {
        if (vin == null) {
            return false;
        }
        vin = sanitizeVin(vin);

        if(!vin.matches("[ABCDEFGHJKLMNPRSTUVWXYZ1234567890]{17}")){
            return false;
        }
        char nine = vin.charAt(9);
        if(nine=='U' || nine=='Z' || nine=='0'){
            return false;
        }

        return !("United States".equals(getCountry(vin))) || passesChecksum(vin);
    }

    public String getMake(String vin){
        vin = sanitizeVin(vin);
        String m = makeSet.get(vin.substring(0, 3));
        //some manufacturers own an entire 2-digit block
        return m==null ? makeSet.get(vin.substring(0, 2)) : m;
    }

    public String getCountry(String vin){
        return countrySet.get(sanitizeVin(vin).substring(0, 2));
    }

    public String getRegion(String vin){
        return regionSet.get(sanitizeVin(vin).substring(0, 1));
    }

    public int getYear(String vin){
        // years char can not be U,Z, or 0
        int offset = "ABCDEFGHJKLMNPRSTVWXY123456789".indexOf(vin.charAt(9));
        if(isNumeric(vin.charAt(6))){
            //pre-2010
            return 1980 + offset; //A==1980 ... 9==2009
        }else{
            //post-2010
            return 2010 + offset; //A==2010 ... 9==2039
        }
    }

    private static boolean isNumeric(char c){
        return c >= '0' && c <= '9';
    }

    public String sanitizeVin(String vin){
        return vin.toUpperCase().replace('I', '1').replace('O', '0').replace('Q', '0').replaceAll("\\s", "");
    }

    private void initialize(Context context){
        if(makeSet==null || regionSet==null || countrySet==null){
            try {
                InputStream is = context.getAssets().open("databases/dataset.json");

                // Read the contents of the file
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                JSONObject data = new JSONObject(new String(buffer, "UTF-8"));

                JSONObject makes = data.getJSONObject("manufacturers");
                makeSet = new HashMap<>(makes.length());
                Iterator<String> makeIterator = makes.keys();
                while (makeIterator.hasNext()) {
                    String code = makeIterator.next();
                    String manufacterer = makes.getString(code);
                    makeSet.put(code, manufacterer);
                }

                JSONObject countries = data.getJSONObject("countries");
                countrySet = new HashMap<>(countries.length());
                Iterator<String> countryIterator = countries.keys();
                while (countryIterator.hasNext()) {
                    String code = countryIterator.next();
                    String country = countries.getString(code);
                    countrySet.put(code, country);
                }


                JSONObject regions = data.getJSONObject("regions");
                regionSet = new HashMap<>(regions.length());
                Iterator<String> regionIterator = regions.keys();
                while (regionIterator.hasNext()) {
                    String code = regionIterator.next();
                    String region = regions.getString(code);
                    regionSet.put(code, region);
                }

            }catch (Exception e){
//                Logger.getInstance().logString("VinValidator", "Problem parsing json data");
//                Logger.getInstance().logHandledException(e);
                //set to empty instead
                makeSet = new HashMap<>();
                countrySet = new HashMap<>();
                regionSet = new HashMap<>();
            }
        }
    }
}
