package com.fixdapp.lib.obd;

/**
 * Created by charles on 12/10/15.
 *
 * OBD-II compliant protocols
 */
public enum Protocol {

    J1850_PWM,
    J1850_VPW,
    ISO_9141_2,
    ISO_14230_4_KWP,
    ISO_14230_4_KWP_FAST,
    CAN_11_500,
    CAN_29_500,
    CAN_11_250,
    CAN_29_250;

    public boolean isCAN(){
        switch (this){
            case CAN_11_250:
            case CAN_11_500:
            case CAN_29_250:
            case CAN_29_500:
                return true;
            default:
                return false;
        }
    }

    public boolean isExtendedCAN() {
        return this == CAN_29_500 || this == CAN_29_250;
    }

    public boolean isISO(){
        switch (this){
            case ISO_9141_2:
            case ISO_14230_4_KWP:
            case ISO_14230_4_KWP_FAST:
                return true;
            default:
                return false;
        }
    }

    public boolean isJ1850(){
        return this == J1850_PWM || this == J1850_VPW;
    }
}
