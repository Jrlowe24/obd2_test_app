//package com.fixdapp.lib.obd.mock;
//
//import com.fixdapp.lib.ByteArray;
//import com.fixdapp.lib.obd.exceptions.VehicleNotConnectedException;
//
///**
// * Created by cjk on 9/14/16.
// */
//
//class NonCANMockELMInterface extends MockELMInterface {
//
//    private String prefix;
//
//    NonCANMockELMInterface(Config config) throws VehicleNotConnectedException {
//        super(config);
//        switch (config.protocol){
//            case J1850_PWM:
//                prefix = "41 6B";
//                break;
//            case ISO_9141_2:
//            case J1850_VPW:
//                prefix = "48 6B";
//                break;
//            case ISO_14230_4_KWP://TODO haven't confirmed this
//            case ISO_14230_4_KWP_FAST:
//                prefix = "86 F1";
//                break;
//        }
//    }
//
//    private String prefix(){
//        return prefix + " ";
//    }
//
//    private String randomFooter(){
//        //TODO actually, footers aren't random, they are consistant for a particular response (but different for each protocol)
//        return ByteArray.bytesToHex((byte)(0xFF & (Math.round(Math.random()*256))));
//    }
//
//    @Override
//    String s03() {
//        return  prefix()+"10 43 01 00 02 00 03 00 "+randomFooter()+"\r" +
//                prefix()+"10 43 43 00 82 00 C1 00 "+randomFooter()+"\r" +
//                prefix()+"18 43 01 01 00 00 00 00 "+randomFooter();
//    }
//
//    @Override
//    String s04() {
//        return  prefix()+"10 44 "+randomFooter()+"\r" +
//                prefix()+"18 44 "+randomFooter()+"\r" +
//                prefix()+"28 44 "+randomFooter();
//    }
//
//    @Override
//    String s0101(boolean celOn) {
//        return  prefix()+"10 41 01 "+(celOn ? "86": "00")+" 07 EF 80 "+randomFooter()+"\r" +
//                prefix()+"18 41 01 "+(celOn ? "86": "00")+" 00 00 00 "+randomFooter()+"\r" +
//                prefix()+"28 41 01 "+(celOn ? "86": "00")+" 00 00 00 "+randomFooter();
//    }
//
//    @Override
//    String s0100() {
//        return  prefix()+"10 41 00 BE 1B 30 13 "+randomFooter()+"\r" +
//                prefix()+"18 41 00 88 18 00 10 "+randomFooter()+"\r" +
//                prefix()+"28 41 00 80 08 00 10 "+randomFooter();
//    }
//
//    @Override
//    String s0121() {
//        return prefix()+"10 41 21 03 E8 "+randomFooter();
//    }
//
//    @Override
//    String s0131() {
//        return  prefix()+"10 41 31 01 33 "+randomFooter()+"\r"+
//                prefix()+"18 41 31 FF FF "+randomFooter();
//    }
//
//    @Override
//    String sMileageInvalid() {
//        return prefix()+"10 41 AA "+randomFooter(); // only one byte instead of two
//    }
//
//    @Override
//    String sMileageError() {
//        return "UNABLE TO CONNECT";
//    }
//
//    @Override
//    String s0902(VINResponseType type) {
//        switch (type){
//            case VALID:
//                return  prefix()+"10 49 02 01 00 00 00 31 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 02 47 31 4A 43 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 03 35 34 34 34 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 04 52 37 32 35 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 05 32 33 36 37 "+randomFooter();
//            case ALL_FFS:
//                return  prefix()+"10 49 02 01 FF FF FF FF "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 02 FF FF FF FF "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 03 FF FF FF FF "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 04 FF FF FF FF "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 05 FF FF FF FF "+randomFooter();
//            case ALL_ZEROES:
//                return  prefix()+"10 49 02 01 00 00 00 00 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 02 00 00 00 00 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 03 00 00 00 00 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 04 00 00 00 00 "+randomFooter()+"\r" +
//                        prefix()+"10 49 02 05 00 00 00 00 "+randomFooter();
//            case NONE:
//                return "NO DATA";
//            case ERROR:
//                return "UNABLE TO CONNECT"; //TODO is this really what it says?
//            default:
//                return null;
//        }
//    }
//
//    /*
//        responses.put("010D",
//        "48 6B 10 41 0D 80 EC\r" +
//        "48 6B 18 41 0D 80 70\r" +
//        "48 6B 28 41 0D 80 1F");
//
//        responses.put("020000",
//        "48 6B 10 42 00 00 48 18 00 00 F1");
//
//        responses.put("0900",
//        "48 6B 10 49 00 01 FC 00 00 00 AE");
//     */
//}