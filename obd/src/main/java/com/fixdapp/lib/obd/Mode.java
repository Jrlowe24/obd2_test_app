package com.fixdapp.lib.obd;

/**
 * Created by charles on 12/9/15.
 */
public enum Mode {
    CURRENT_DATA(0x01),         // get the current value of a particular gauge
    FREEZE_DATA(0x02),          // the the historical value of a particular gauge at last MIL event
    STORED_DTCS(0x03),          // get the current (stored) DTCs
    CLEAR_STORED_DTCS(0x04),    // clear the current (stored) DTCs
    //        TEST_NON_CAN_02(0x05),      // get information about oxygen sensors. applies only to non-CAN vehicles
//        TEST(0x06),                 // Test results, other component/system monitoring (Test results, oxygen sensor monitoring for CAN only)
    PENDING_DTCS(0x07),         // get the pending DTCs
    //        CONTROL(0x08),              // Control operation of on-board component/system
    VEHICLE_INFO(0x09),         // get basic vehicle information
    PERMANENT_DTCS(0x0A);      // get permanent DTCs

    private byte code;

    Mode(int code){
        this.code = (byte) code;
    }

    public byte getCode(){
        return code;
    }

    public String getHexCode(){
        return bytesToHex(new byte[]{code});
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
