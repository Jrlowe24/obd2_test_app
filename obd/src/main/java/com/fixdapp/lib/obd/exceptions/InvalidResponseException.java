package com.fixdapp.lib.obd.exceptions;

/**
 * Created by  Charles Julian Knight, julian@fixdapp.com  on 6/13/16.
 */
public class InvalidResponseException extends Exception {
    public InvalidResponseException(String message){
        super(message);
    }
    public InvalidResponseException(Throwable e){
        super(e);
    }
}