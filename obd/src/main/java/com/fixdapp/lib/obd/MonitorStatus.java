package com.fixdapp.lib.obd;

import com.fixdapp.lib.ByteArray;

import java.util.List;

/**
 * Created by charles on 12/8/15.
 *
 * return result for {@link PID#MONITOR_STATUS_SINCE_CLEARED}
 */
public class MonitorStatus {

    /**
     * Possible OBD-II tests
     */
    public enum MonitorTest {
        MISFIRE              ("B0", "B4", true,  true),
        FUELSYSTEM           ("B1", "B5", true,  true),
        COMPONENTS           ("B2", "B6", true,  true),
        CATALYST             ("C0", "D0", true,  false),
        HEATED_CATALYST      ("C1", "D1", true,  false),
        EVAPORATIVE_SYSTEM   ("C2", "D2", true,  false),
        SECONDARY_AIR_SYSTEM ("C3", "D3", true,  false),
        AC_REFRIGERANT       ("C4", "D4", true,  false),
        OXYGEN_SENSOR        ("C5", "D5", true,  false),
        OXYGEN_SENSOR_HEATER ("C6", "D6", true,  false),
        EGR_SYSTEM           ("C7", "D7", true,  false),
        NMHC_CATALYST        ("C0", "D0", false, true),
        NOX_SCR_MONITOR      ("C1", "D2", false, true),
        BOOST_PRESSURE       ("C3", "D4", false, true),
        EXHAUST_GAS_SENSOR   ("C5", "D5", false, true),
        PM_FILTER_MONITORING ("C6", "D6", false, true),
        EGR_VVT_SYSTEM       ("C7", "D7", false, true);

        private int availabilityByteIndex, availabilityBitIndex, incompletenessByteIndex, incompletenessBitIndex;
        private boolean forSparkIgnitions, forCompressionIgnitions;

        /**
         * @param availabilityBit the bit that stores if the test is available in this vehicle
         * @param incompletenessBit the bit that stores if the test is in progress
         * @param forSparkIgnitions does this test apply to spark ignitions?
         * @param forCompressionIgnitions does this test apply to compression ignitions?
         */
        MonitorTest(String availabilityBit, String incompletenessBit, boolean forSparkIgnitions, boolean forCompressionIgnitions){
            this.availabilityByteIndex = availabilityBit.charAt(0)-'A';
            this.availabilityBitIndex = Integer.parseInt(availabilityBit.substring(1));
            this.incompletenessByteIndex = incompletenessBit.charAt(0)-'A';
            this.incompletenessBitIndex = Integer.parseInt(incompletenessBit.substring(1));
            this.forSparkIgnitions = forSparkIgnitions;
            this.forCompressionIgnitions = forCompressionIgnitions;
        }
    }

    private ByteArray rawData;
    private boolean isCurrent;
    int dtcCount;

    public MonitorStatus(List<PIDResponse> responses){
        dtcCount = 0;
//        byte mode = responses.get(0).getRequestedMode();
        for(PIDResponse response : responses){
//            if(response.getRequestedMode() != mode){
//                throw new IllegalArgumentException("All modes must be the same");
//            }
            dtcCount += response.getData().getBits(0, 0, 6); // A6-A0
        }
        //for most of the status data, we can mask all the responses together (for tests, etc).
        // however, we want to sum the number of DTCs across all responses
        rawData = PIDResponse.reduceByMask(responses);
        rawData.raw()[0] = (byte) ((rawData.raw()[0] & 0x80) + dtcCount);
    }

    public boolean isCelOn() {
        if(!isCurrent){
            throw new IllegalArgumentException("This is only available in PID 01");
        }
        return rawData.getBit(0, 7); // A7
    }

    public int getDtcCount() {
        if(!isCurrent){
            throw new IllegalArgumentException("This is only available in PID 01");
        }
        return dtcCount;
    }

    public boolean usesSparkIgnition() {
        return !usesCompressionIgnition();
    }

    public boolean usesCompressionIgnition() {
        return rawData.getBit(1, 3); //B3
    }

    /**
     * True if test is possible for this vehicle
     */
    public boolean isTestAvailable(MonitorTest test){
        return ((usesSparkIgnition() && test.forSparkIgnitions)
                || (usesCompressionIgnition() && test.forCompressionIgnitions))
                && rawData.getBit(test.availabilityByteIndex, test.availabilityBitIndex);
    }

    /**
     * True if test is available and not currently running
     */
    public boolean isTestRunning(MonitorTest test) {
        return !(test.forSparkIgnitions && usesCompressionIgnition())
                && !(test.forCompressionIgnitions && usesSparkIgnition())
                && rawData.getBit(test.incompletenessByteIndex, test.incompletenessBitIndex);
    }
}
