package com.fixdapp.lib.obd.mock;

import com.fixdapp.android.logger.LogManager;
import com.fixdapp.lib.obd.ELMOBDIIInterface;
import com.fixdapp.lib.obd.Protocol;
import com.fixdapp.lib.obd.exceptions.InvalidResponseException;
import com.fixdapp.lib.obd.exceptions.VehicleNotConnectedException;
import com.fixdapp.lib.sensor.DataInterface;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okio.ByteString;

/**
 * Created by charles on 11/30/15.
 *
 * A mock ELMOBDIIInterface where the return results are based off the ECUSim 2000 responses with
 * headers enabled.
 */
public class MockELMInterface extends ELMOBDIIInterface {

    public static class Builder {

        private Config config = new Config();

        public Builder(Protocol protocol, LogManager logManager, JSONObject responses){
            this.config.protocol = protocol;
            this.config.logManager = logManager;
            this.config.responses = responses;
        }

        public Builder setHasDtcs(boolean hasDtcs){
            config.hasDtcs = hasDtcs;
            return this;
        }

        public Builder setSimulateTime(boolean simulateTime){
            config.simulateTime = simulateTime;
            return this;
        }

        public Builder throwOnStart(){
            config.throwsOnStart = true;
            return this;
        }

        public Builder setVINResponse(VINResponseType type){
            config.vinResponseType = type;
            return this;
        }

        public Builder setMileageResponse(MileageResponseType type){
            config.mileageResponseType = type;
            return this;
        }

        public Builder addCustomResponse(String request, String response) {
            try {
                config.responses.put(request, response);
            } catch (JSONException e) {
                throw new IllegalArgumentException(e);
            }
            return this;
        }

        public MockELMInterface build() throws VehicleNotConnectedException {
            return new MockELMInterface(config);
        }
    }

    static class Config {
        LogManager logManager;
        Protocol protocol;
        JSONObject responses;
        boolean hasDtcs = true;
        boolean simulateTime = false;
        boolean throwsOnStart = false;
        VINResponseType vinResponseType = VINResponseType.VALID;
        MileageResponseType mileageResponseType = MileageResponseType.BOTH;
    }

    public enum MileageResponseType {
        NONE,
        ERROR,
        INVALID,
        MIL,
        CLEAR,
        BOTH
    }

    public enum VINResponseType {
        NONE,
        ERROR,
        INVALID,
        ALL_FFS,
        ALL_ZEROES,
        VALID
    }

    private Config config;
    private boolean hasDoneFirstSetup = false;

    MockELMInterface(Config config) throws VehicleNotConnectedException {
        super(new DataInterface() {
            @NotNull
            @Override
            public ByteString write(@NotNull ByteString data, byte terminator) {
                return ByteString.EMPTY;
            }

            @Override
            public void close() {

            }
        }, config.logManager);
        this.config = config;
        maybeSleep(1000);
        if(config.throwsOnStart){
            throw new VehicleNotConnectedException();
        }
    }

    public boolean isSimulatingTime() {
        return config.simulateTime;
    }

    private void maybeSleep(long millis) {
        if(!isSimulatingTime()) return;
        try {
            Thread.sleep(millis);
        }catch (InterruptedException e){
            // oh well
        }
    }

    @Override
    public void close() throws IOException {
        // no-op
    }

    private String ATDPN(){
        switch (config.protocol){
            case J1850_PWM:
                return "A1";
            case J1850_VPW:
                return "A2";
            case ISO_9141_2:
                return "A3";
            case ISO_14230_4_KWP:
                return "A4";
            case ISO_14230_4_KWP_FAST:
                return "A5";
            case CAN_11_500:
                return "A6";
            case CAN_29_500:
                return "A7";
            case CAN_11_250:
                return "A8";
            case CAN_29_250:
                return "A9";
            default:
                return null;
        }
    }

    @Override
    public String sendCommand(String request) throws IOException {
        getLogger().tag("ELM").i("SEND:", request.replace("\r", "\\r")); //encode \r so logger doesn't freak out
        request = request.toUpperCase().replaceAll("\\s", "");

        StringBuilder b = new StringBuilder();
        boolean isATCommand = request.startsWith("AT");

        if(!isATCommand && !hasDoneFirstSetup){
            hasDoneFirstSetup = true;
            b.append("SEARCHING...\r");
            maybeSleep(1000);
        }
        if(request.equals("ATDP")) { //detect protocol
            b.append("AUTO, ").append(config.protocol.toString());
        }else if(request.equals("ATDPN")) { //protocol number
            b.append(ATDPN());
        }else if(request.equals("ATZ")){
            maybeSleep(1500);
            b.append("\rELM327 v1.5");
        }else if(isATCommand) {
            maybeSleep(500);
            b.append("OK");
        }else{
            maybeSleep(1000);
            try {
                if(config.responses.has(request) && config.responses.get(request) instanceof String) {
                    b.append(config.responses.getString(request));
                } else {
                    switch (request) {
                        case "0101":
                            b.append(config.responses.getJSONObject("0101").getString(
                                    config.hasDtcs ? "cel_on" : "cel_off"
                            ));
                            break;
                        case "0121":
                            switch (config.mileageResponseType) {
                                case NONE:
                                case CLEAR:
                                    b.append("NO DATA\r\r");
                                    break;
                                case INVALID:
                                    b.append(config.responses.getJSONObject("0121").getString("invalid"));
                                    break;
                                case ERROR:
                                    b.append(config.responses.getJSONObject("0121").getString("error"));
                                    break;
                                case BOTH:
                                case MIL:
                                    b.append(config.responses.getJSONObject("0121").getString("valid"));
                                    break;
                            }
                            break;
                        case "0131":
                            switch (config.mileageResponseType) {
                                case NONE:
                                case MIL:
                                    b.append("NO DATA\r\r");
                                    break;
                                case INVALID:
                                    b.append(config.responses.getJSONObject("0131").getString("invalid"));
                                case ERROR:
                                    b.append(config.responses.getJSONObject("0131").getString("error"));
                                    break;
                                case BOTH:
                                case CLEAR:
                                    b.append(config.responses.getJSONObject("0131").getString("valid"));
                                    break;
                            }
                            break;
                        case "03":
                            if (config.hasDtcs) {
                                b.append(config.responses.getString("03"));
                            } else {
                                b.append("NO DATA\r\r");
                            }
                            break;
                        case "04":
                            b.append(config.responses.getString("04"));
                            config.hasDtcs = false; //clear codes
                            break;
                        case "0902":
                            switch (config.vinResponseType) {
                                case VALID:
                                    b.append(config.responses.getJSONObject("0902").getString("valid"));
                                    break;
                                case NONE:
                                    b.append(config.responses.getJSONObject("0902").getString("none"));
                                    break;
                                case ERROR:
                                    b.append(config.responses.getJSONObject("0902").getString("error"));
                                    break;
                                case ALL_FFS:
                                    b.append(config.responses.getJSONObject("0902").getString("all_ffs"));
                                    break;
                                case ALL_ZEROES:
                                    b.append(config.responses.getJSONObject("0902").getString("all_zeros"));
                                    break;
                                case INVALID:
                                    b.append(config.responses.getJSONObject("0902").getString("invalid"));
                                    break;
                            }
                            break;
                        default:
                            b.append("NO DATA\r\r");
                            break;
                    }
                }
            }catch (JSONException e) {
                throw new IOException(e);
            }
        }

        b.append(">");
        getLogger().tag("ELM").i("RECV:", b.toString().replace("\r", "\\r"));
        return b.toString().replace(">", "");
    }
}
