package com.fixdapp.lib.obd.exceptions;

/**
 * Created by matth on 11/16/2015.
 */
public class BogusDtcException extends Exception {
    public BogusDtcException(String bogusDtc) {
        super(bogusDtc);
    }
}
