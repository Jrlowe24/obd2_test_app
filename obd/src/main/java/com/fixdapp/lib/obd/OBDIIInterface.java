package com.fixdapp.lib.obd;

import android.util.Pair;
import android.util.SparseArray;

import com.fixdapp.android.logger.Logger;
import com.fixdapp.lib.ByteArray;
import com.fixdapp.lib.obd.exceptions.InvalidResponseException;
import com.fixdapp.lib.obd.exceptions.VehicleNotConnectedException;

import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Julian on 12/9/2015.
 *
 * An OBDIIInterface separates OBDII operations from the communication method, and translates
 * PIDResponses into useful data.
 */
public abstract class OBDIIInterface implements Closeable {

    //TODO separate out Communication issues from Connection issues

    /**
     * Set up the device so that it's ready for us to check for/pull codes/etc
     */
    public OBDIIInterface() throws VehicleNotConnectedException {

    }

    abstract Logger getLogger();

    /**
     * cache the list of supported PIDs for efficiency.
     */
    private SparseArray<List<PID>> supportedPids = new SparseArray<>();

    /**
     * Write an OBD-II command to the vehicle.
     * @param  mode the requested mode
     * @param pid the requested pid
     * @param id a hex string address of the module you want a response from. Set to null to get responses
     *           from all modules
     * @param data the extra data to add to the request (if applicable). e.g. frameIndex for Mode 02
     * @return a list of responses from all the responding modules. size should be one if only one ECU
     *              responded and 0 (NOT null) if no responses were received. Multiline responses
     *              from the same module should be combined into the same PIDResponse.
     */
    public abstract List<PIDResponse> sendMessage(Mode mode, Byte pid, @Nullable String id,
                                                  @Nullable byte[] data)
            throws VehicleNotConnectedException, InvalidResponseException;

    public List<PIDResponse> sendMessage(Mode mode, PID pid)
            throws VehicleNotConnectedException, InvalidResponseException {
        return sendMessage(mode, pid.getCode(), null, null);
    }

    public abstract List<PIDResponse> sendMessageHex(String mode, String pid, @Nullable String id, @Nullable byte[] data)
            throws VehicleNotConnectedException, InvalidResponseException;

    public abstract StringBuffer readBroadcastData(String id)
            throws VehicleNotConnectedException, InvalidResponseException;

    public List<PIDResponse> sendMessageHex(String mode, String pid)
            throws VehicleNotConnectedException, InvalidResponseException {
        return sendMessageHex(mode, pid, null, null);
    }
    public void setProtocol(int n) throws VehicleNotConnectedException {

    }

    public void reset() throws VehicleNotConnectedException, InvalidResponseException {

    }


    /**
     * Send a command to see if connectivity is still available
     */
    public abstract void checkConnected() throws IOException;

    public List<PID> getSupportedPidsCurrent() throws VehicleNotConnectedException, InvalidResponseException {
        if (supportedPids.get(-1) == null) {
            supportedPids.put(-1, getSupportedPIDs(Mode.CURRENT_DATA, new byte[0]));
        }
        return supportedPids.get(-1);
    }

    public List<PID> getSupportedPidsFreeze(int dtcIndex) throws VehicleNotConnectedException, InvalidResponseException {
        if (supportedPids.get(dtcIndex) == null) {
            supportedPids.put(dtcIndex, getSupportedPIDs(Mode.FREEZE_DATA, new byte[] { (byte) (dtcIndex + 1) }));
        }
        return supportedPids.get(dtcIndex);
    }

    /**
     * Get a list of all the PIDs supported for a particular mode
     */
    private List<PID> getSupportedPIDs(Mode mode, byte[] requestData)
            throws VehicleNotConnectedException, InvalidResponseException {

        if (mode != Mode.CURRENT_DATA && mode != Mode.FREEZE_DATA) {
            throw new IllegalArgumentException("Can't get supported freeze frames for mode " + mode);
        }
        List<PID> supported = new ArrayList<>();
        for (PID supportedQueryPID : PID.SUPPORTED_PID_DATA_PIDS) {
            List<PIDResponse> responses = sendMessage(mode, supportedQueryPID.getCode(), null, requestData); //query for which PIDs are supported in this range
//            int bytesReturned = supportedQueryPID.getBytesReturned();
            if (responses.size() > 0) {
                ByteArray data = PIDResponse.reduceByMask(responses);
                for (int j = 0; j < (4 /* bytes */ * 8 /* bits per byte */); j++) {
                    if (data.getBit(j)) {
                        PID p = PID.get(mode, (byte) (supportedQueryPID.getCode() + 1 + j)); //get the PID for this mode and index
                        if (p != null) {
                            supported.add(p);
                        }
                    }

                }
            }
        }
        getLogger().d("Found", supported.size(), "PIDs supported for Mode", mode);
        return supported;
    }

    public void cleanUp() throws IOException {

    }

    public abstract void close() throws IOException;

    /**
     * Get the number of Stored DTCs
     */
    public int getDtcCount() throws VehicleNotConnectedException, InvalidResponseException {
        List<PIDResponse> data = sendMessage(Mode.CURRENT_DATA, PID.MONITOR_STATUS_SINCE_CLEARED);
        return new MonitorStatus(data).getDtcCount();
    }

    /**
     * determine if the Check Engine Light is on
     */
    public boolean isCelOn() throws VehicleNotConnectedException, InvalidResponseException {
        List<PIDResponse> data = sendMessage(Mode.CURRENT_DATA, PID.MONITOR_STATUS_SINCE_CLEARED);
        return new MonitorStatus(data).isCelOn();
    }

    public MonitorStatus getCurrentMonitorStatus() throws VehicleNotConnectedException, InvalidResponseException {
        List<PIDResponse> responses = sendMessage(Mode.CURRENT_DATA, PID.MONITOR_STATUS_SINCE_CLEARED);
        if (responses.isEmpty()) {
            return null;
        }
        return new MonitorStatus(responses);
    }

    /**
     * get the stored (mode 03) diagnostic trouble codes
     */
    public DtcSet getStoredDtcs() throws VehicleNotConnectedException, InvalidResponseException {
        Integer expectedCount = null;
        Boolean celOn = null;
        try {
            MonitorStatus status = getCurrentMonitorStatus();
            if (status != null) {
                expectedCount = status.getDtcCount();
                celOn = status.isCelOn();
            }
        }catch (Throwable e) {
            getLogger().w(e, "Problem reading monitor status");
        }
        Set<DtcCode> dtcs = getDtcs(Mode.STORED_DTCS);
        if (expectedCount != null && dtcs.size() != expectedCount) {
            getLogger().w("Monitor status reported", expectedCount, "dtcs but found", dtcs.size());
        }
        if (celOn != null && (dtcs.size() > 0) != celOn) {
            getLogger().w("cel was reported as", celOn ? "on" : "off", "but there were", dtcs.size(), "dtcs");
        }
        boolean celState = celOn == null ? dtcs.size() > 0 : celOn;
        return new DtcSet(dtcs, celState);
    }

    public Set<DtcCode> getPendingDtcs() throws VehicleNotConnectedException, InvalidResponseException {
        return getDtcs(Mode.PENDING_DTCS);
    }

    public Set<DtcCode> getPermanentDtcs() throws VehicleNotConnectedException, InvalidResponseException {
        return getDtcs(Mode.PERMANENT_DTCS);
    }

    private Set<DtcCode> getDtcs(Mode mode) throws VehicleNotConnectedException, InvalidResponseException {
        if (mode != Mode.STORED_DTCS && mode != Mode.PENDING_DTCS && mode != Mode.PERMANENT_DTCS) {
            throw new IllegalArgumentException("Mode must be pending or permanent");
        }
        Set<DtcCode> dtcs = new HashSet<>();
        List<PIDResponse> responses = sendMessage(mode, PID.GET_DTCS);
        for(PIDResponse response : responses){
            int expectedDtcs = response.getItemCount() != null ? response.getItemCount() : response.getData().size() / 2;
            for(int i = 0; i < (2 * expectedDtcs); i += 2){
                // if there are an even number of bytes left and the bytes aren't 0x0000
                if(response.getData().size()-i >= 2 && response.getData().getUnsignedShort(i) != 0){
                    dtcs.add(new DtcCode(unsignedShortToDtcCode(response.getData(), i)));
                }
            }
        }
        return dtcs;
    }

    /**
     * Clear the shared DTCs, CEL, and any Freeze Frame data.
     *
     * <b>Dangerous!</b>
     */
    public void clearCodes() throws VehicleNotConnectedException, InvalidResponseException {
        supportedPids.clear();
        sendMessage(Mode.CLEAR_STORED_DTCS, PID.CLEAR_DTCS);
    }

    /**
     * Get the Vehicle Identification Number, if available
     */
    @Nullable
    public String getVin() throws VehicleNotConnectedException, InvalidResponseException {
        List<PIDResponse> responses = sendMessage(Mode.VEHICLE_INFO, PID.VIN);
        if(responses.isEmpty()){
            return null; //no vin available
        }
        PIDResponse vinResponse = responses.get(0); //only one module should respond with a vin

        //remove any null/FF bytes (filler data)
        String vin = vinResponse.getData().getAsASCIIString().toUpperCase().replaceAll("[^A-Z0-9]", "");
        if(!vin.matches("^[ABCDEFGHJKLMNPRSTUVWXYZ1234567890]{12}[0-9]{5}")){
            getLogger().w("Found invalid vin", vin);
            return null;
        }else{
            return vin.substring(0, 17);
        }
    }

    /**
     * Get the Vehicle Check to identify basic vehicle info, if available
     */
    @Nullable
    public void getVehicleCheck() throws VehicleNotConnectedException, InvalidResponseException {
        sendMessage(Mode.VEHICLE_INFO, PID.CALIBRATION_ID_MESSAGE_COUNT);
        sendMessage(Mode.VEHICLE_INFO, PID.CALIBRATION_ID);
        sendMessage(Mode.VEHICLE_INFO, PID.CVN_MESSAGE_COUNT);
        sendMessage(Mode.VEHICLE_INFO, PID.CVN);
        sendMessage(Mode.VEHICLE_INFO, PID.PERF_TRACKING_MESSAGE_COUNT);
        sendMessage(Mode.VEHICLE_INFO, PID.PERF_TRACKING_SPARK);
        sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME_MESSAGE_COUNT);
        sendMessage(Mode.VEHICLE_INFO, PID.ECU_NAME);
        sendMessage(Mode.VEHICLE_INFO, PID.PERF_TRACKING_COMPRESSION);
        sendMessage(Mode.CURRENT_DATA, PID.FUEL_SYSTEM_STATUS);
        sendMessage(Mode.CURRENT_DATA, PID.STANDARDS_COMPLIANCE);
        sendMessage(Mode.CURRENT_DATA, PID.FUEL_TYPE);
        sendMessage(Mode.CURRENT_DATA, PID.RUNTIME_SINCE_ENGINE_START);
        sendMessage(Mode.CURRENT_DATA, PID.RUNTIME_WITH_MIL_ON);
        sendMessage(Mode.CURRENT_DATA, PID.RUNTIME_SINCE_CODES_CLEARED);
        sendMessage(Mode.CURRENT_DATA, PID.ENGINE_RUNTIME);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_1);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_2);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_3);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_4);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_5);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_6);
        sendMessage(Mode.CURRENT_DATA, PID.DATA_PIDS_SUPPORTED_7);

    }

    @Nullable
    public String getFingerprint() throws VehicleNotConnectedException, InvalidResponseException {
        List<PIDResponse> responses = new ArrayList<>();
        for (PID supportedQueryPID : PID.SUPPORTED_PID_DATA_PIDS) {
            List<PIDResponse> r = sendMessage(Mode.CURRENT_DATA, supportedQueryPID.getCode(), null, new byte[0]);
            responses.addAll(r);
        }
        byte[] bytes = new byte[8 * 4];
        for (PIDResponse response : responses) {
            int indexOffset = (response.getRequestedPIDInt() & 0xFF) / 8;
            byte[] data = response.getData().raw();
            for(int i = 0; i < Math.min(data.length, 4); i++) {
                bytes[indexOffset + i] |= data[i];
            }
        }
        // return data in hex, and truncate trailing zero bytes
        return ByteArray.bytesToHex(bytes).replaceAll("(00)+$", "");
    }

    @Nullable
    public List<Pair<String,Integer>> getMilesSinceClear() throws VehicleNotConnectedException, InvalidResponseException {
        return getAllUnsignedShorts(PID.DISTANCE_SINCE_CODES_CLEARED);
    }

    @Nullable
    public List<Pair<String,Integer>> getMilesWithMIL() throws VehicleNotConnectedException, InvalidResponseException {
        return getAllUnsignedShorts(PID.DISTANCE_WITH_MIL_ON);
    }

    @Nullable
    private List<Pair<String,Integer>> getAllUnsignedShorts(PID pid) throws VehicleNotConnectedException, InvalidResponseException {
        List<PIDResponse> responses = sendMessage(Mode.CURRENT_DATA, pid);
        if (responses.isEmpty()) {
            return Collections.emptyList();
        }
        try {
            List<Pair<String, Integer>> results = new ArrayList<>();
            for (PIDResponse response : responses) {
                int val = response.getData().getUnsignedShort(0);
                if (val > 0 && val < 65535) {
                    results.add(new Pair<>(response.getResponderId(), val));
                }
            }
            return results;
        }catch (Exception e){
            throw new InvalidResponseException(e);
        }
    }

    /**
     * get the protocol currently in use to communicate with the vehicle
     */
    public abstract Protocol getProtocol() throws VehicleNotConnectedException, InvalidResponseException;

    /**
     * get two bytes from a ByteArray and parse them as a DTC code
     */
    public static String unsignedShortToDtcCode(ByteArray data, int position) throws InvalidResponseException {
        try {
            char[] code = new char[5];
            code[0] = "PCBU".toCharArray()[data.getBits(position, 6, 7)];   // A7-6 are first character
            code[1] = HEX_ARRAY[data.getBits(position, 4, 5)];             // A5-A4 are 0-3
            code[2] = HEX_ARRAY[data.getBits(position, 0, 3)];             // the rest of A is hex
            code[3] = HEX_ARRAY[data.getBits(position + 1, 4, 7)];            // all of B is hex
            code[4] = HEX_ARRAY[data.getBits(position + 1, 0, 3)];            // all of B is hex
            return new String(code);
        }catch (Exception e){
            throw new InvalidResponseException(e);
        }
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
}
