package com.fixdapp.lib.obd;

import org.jetbrains.annotations.Nullable;

/**
 * Created by charles on 12/9/15.
 *
 * Constants for SAE J1979 OBD-II protocol.
 *
 * TODO add PIDs for modes 05, 06 and 08
 *
 * @see <a href="https://fixd.slack.com/files/john/F0G3S4SRX/sae--j2012-2007.pdf">Spec</a>
 * @see <a href="https://en.wikipedia.org/wiki/OBD-II_PIDs">Wikipedia</a>
 */
public enum PID {

    //PIDs for standard gauges (modes 01 and 02)
    DATA_PIDS_SUPPORTED_1          (0x00, 4),    //PIDs supported [01 - 20]                                                       Bit encoded [A7..D0] == [PID $01..PID $20]
    MONITOR_STATUS_SINCE_CLEARED   (0x01, 4),    //Monitor status since DTCs cleared, includes MIL, DTC count                     Bit encoded
    FREEZE_DTC                     (0x02, 2),    //Freeze DTC
    FUEL_SYSTEM_STATUS             (0x03, 2),    //Fuel system status                                                             Bit encoded
    CALCULATED_ENGINE_LOAD         (0x04, 1),    //Calculated engine load value                                                   0 100 % A*100/255
    ENGINE_COOLANT_TEMP_1          (0x05, 1),    //Engine coolant temperature                                                     -40 215 °C A-40
    FTRIM_SHORT_1                  (0x06, 1),    //Short term fuel % trim—Bank 1                                                  -100 Subtracting Fuel (Rich Condition) 99.22 Adding Fuel (Lean Condition) % (A-128) * 100/128
    FTRIM_LONG_1                   (0x07, 1),    //Long term fuel % trim—Bank 1                                                   -100 Subtracting Fuel (Rich Condition) 99.22 Adding Fuel (Lean Condition) % (A-128) * 100/128
    FTRIM_SHORT_2                  (0x08, 1),    //Short term fuel % trim—Bank 2                                                  -100 Subtracting Fuel (Rich Condition) 99.22 Adding Fuel (Lean Condition) % (A-128) * 100/128
    FTRIM_LONG_2                   (0x09, 1),    //Long term fuel % trim—Bank 2                                                   -100 Subtracting Fuel (Rich Condition) 99.22 Adding Fuel (Lean Condition) % (A-128) * 100/128
    FUEL_PRESSURE                  (0x0A, 1),    //Fuel pressure                                                                  0 765 kPa (gauge) A*3
    INTAKE_MANIFOLD_PRESSURE_ABS_1 (0x0B, 1),    //Intake manifold absolute pressure                                              0 255 kPa (absolute) A
    ENGINE_RPM                     (0x0C, 2),    //Engine RPM                                                                     0 16383.75 rpm ((A*256)+B)/4
    VEHICLE_SPEED                  (0x0D, 1),    //Vehicle speed                                                                  0 255 km/h A
    TIMING_ADVANCE                 (0x0E, 1),    //Timing advance                                                                 -64 63.5 ° relative to #1 cylinder (A-128)/2
    INTAKE_AIR_TEMP_1              (0x0F, 1),    //Intake air temperature                                                         -40 215 °C A-40
    MAF_AIR_FLOW_RATE              (0x10, 2),    //MAF air flow rate                                                              0 655.35 grams/sec ((A*256)+B) / 100
    THROTTLE_POS_A                 (0x11, 1),    //Throttle position                                                              0 100 % A*100/255
    COMMANDED_SECONDARY_AIR        (0x12, 1),    //Commanded secondary air status                                                 Bit encoded
    O2_SENSORS_PRESENT_1           (0x13, 1),    //Oxygen sensors present                                                         [A0..A3] == Bank 1, Sensors 1-4. [A4..A7] == Bank 2...
    O2_VOLTAGE_FTRIM_SHORT_1_1     (0x14, 2),    //Bank 1, Sensor 1 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_1_2     (0x15, 2),    //Bank 1, Sensor 2 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_1_3     (0x16, 2),    //Bank 1, Sensor 3 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_1_4     (0x17, 2),    //Bank 1, Sensor 4 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_2_1     (0x18, 2),    //Bank 2, Sensor 1 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_2_2     (0x19, 2),    //Bank 2, Sensor 2 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_2_3     (0x1A, 2),    //Bank 2, Sensor 3 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    O2_VOLTAGE_FTRIM_SHORT_2_4     (0x1B, 2),    //Bank 2, Sensor 4 voltage, Short fuel trim                                      0 -100(lean) 1.275 99.2(rich) Volts % A/200 (B-128) * 100/128 (if B==$FF, sensor is not used in trim calc)
    STANDARDS_COMPLIANCE           (0x1C, 1),    //OBD standards this vehicle conforms to                                         Bit encoded
    O2_SENSORS_PRESENT_2           (0x1D, 1),    //Oxygen sensors present                                                          Similar to PID 13, but [A0..A7] == [B1S1, B1S2, B2S1, B2S2, B3S1, B3S2, B4S1, B4S2]
    AUX_INPUT_STATUS               (0x1E, 1),    //Auxiliary input status                                                          A0 == Power Take Off (PTO) status (1 == active) [A1..A7] not used
    RUNTIME_SINCE_ENGINE_START     (0x1F, 2),    //Run time since engine start                                                    0 65535 seconds (A*256)+B
    DATA_PIDS_SUPPORTED_2          (0x20, 4),    //PIDs supported [21 - 40]                                                       Bit encoded [A7..D0] == [PID $21..PID $40]
    DISTANCE_WITH_MIL_ON           (0x21, 2),    //Distance traveled with malfunction indicator lamp (MIL) on                     0 65535 km (A*256)+B
    FUEL_RAIL_PRESSURE_REL         (0x22, 2),    //Fuel rail Pressure (relative to manifold vacuum)                               0 5177.265 kPa ((A*256)+B) * 0.079
    FUEL_RAIL_PRESSURE_DIRECT      (0x23, 2),    //Fuel rail Pressure (diesel, or gasoline direct inject)                         0 655350 kPa (gauge) ((A*256)+B) * 10
    O2_EQUIV_RATIO_VOLTAGE_1_1     (0x24, 4),    //O2S1_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 1.999 7.999 N/A V ((A*256)+B)*2/65535 or ((A*256)+B)/32768 ((C*256)+D)*8/65535 or ((C*256)+D)/8192
    O2_EQUIV_RATIO_VOLTAGE_1_2     (0x25, 4),    //O2S2_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    O2_EQUIV_RATIO_VOLTAGE_1_3     (0x26, 4),    //O2S3_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    O2_EQUIV_RATIO_VOLTAGE_1_4     (0x27, 4),    //O2S4_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    O2_EQUIV_RATIO_VOLTAGE_2_1     (0x28, 4),    //O2S5_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    O2_EQUIV_RATIO_VOLTAGE_2_2     (0x29, 4),    //O2S6_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    O2_EQUIV_RATIO_VOLTAGE_2_3     (0x2A, 4),    //O2S7_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    O2_EQUIV_RATIO_VOLTAGE_2_4     (0x2B, 4),    //O2S8_WR_lambda(1): Equivalence Ratio Voltage                                   0 0 2 8 N/A V ((A*256)+B)*2/65535 ((C*256)+D)*8/65535
    COMMANDED_EGR                  (0x2C, 1),    //Commanded EGR                                                                  0 100 % A*100/255
    EGR_ERROR                      (0x2D, 1),    //EGR Error                                                                      -100 99.22 % (A-128) * 100/128
    COMMANDED_EVAPORATIVE_PURGE    (0x2E, 1),    //Commanded evaporative purge                                                    0 100 % A*100/255
    FUEL_LEVEL_INPUT               (0x2F, 1),    //Fuel Level Input                                                               0 100 % A*100/255
    WARMUPS_SINCE_CODES_CLEARED    (0x30, 1),    //# of warm-ups since codes cleared                                              0 255 N/A A
    DISTANCE_SINCE_CODES_CLEARED   (0x31, 2),    //Distance traveled since codes cleared                                          0 65535 km (A*256)+B
    EVAP_VAPOR_PRESSURE_REL_1      (0x32, 2),    //Evap. System Vapor Pressure                                                    -8192 8192 Pa ((A*256)+B)/4 (A and B are two's complement signed)
    BAROMETRIC_PRESSURE            (0x33, 1),    //Barometric pressure                                                            0 255 kPa (Absolute) A
    O2_EQUIV_RATIO_CURRENT_1_1     (0x34, 4),    //O2S1_WR_lambda(1): Equivalence Ratio Current                                   0 -128 1.999 127.99 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_1_2     (0x35, 4),    //O2S2_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_1_3     (0x36, 4),    //O2S3_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_1_4     (0x37, 4),    //O2S4_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_2_1     (0x38, 4),    //O2S5_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_2_2     (0x39, 4),    //O2S6_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_2_3     (0x3A, 4),    //O2S7_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    O2_EQUIV_RATIO_CURRENT_2_4     (0x3B, 4),    //O2S8_WR_lambda(1): Equivalence Ratio Current                                   0 -128 2 128 N/A mA ((A*256)+B)/32,768 ((C*256)+D)/256 - 128
    CATALYST_TEMP_1_1              (0x3C, 2),    //Catalyst Temperature Bank 1, Sensor 1                                          -40 6513.5 °C ((A*256)+B)/10 - 40
    CATALYST_TEMP_1_2              (0x3D, 2),    //Catalyst Temperature Bank 2, Sensor 1                                          -40 6513.5 °C ((A*256)+B)/10 - 40
    CATALYST_TEMP_2_1              (0x3E, 2),    //Catalyst Temperature Bank 1, Sensor 2                                          -40 6513.5 °C ((A*256)+B)/10 - 40
    CATALYST_TEMP_2_2              (0x3F, 2),    //Catalyst Temperature Bank 2, Sensor 2                                          -40 6513.5 °C ((A*256)+B)/10 - 40
    DATA_PIDS_SUPPORTED_3          (0x40, 4),    //PIDs supported [41 - 60]                                                       Bit encoded [A7..D0] == [PID $41..PID $60]
    MONITOR_STATUS_CURRENT         (0x41, 4),    //Monitor status this drive cycle                                                Bit encoded
    CONTROL_MODULE_VOLTAGE         (0x42, 2),    //Control module voltage                                                         0 65.535 V ((A*256)+B)/1000
    ABS_LOAD                       (0x43, 2),    //Absolute load value                                                            0 25700 % ((A*256)+B)*100/255
    COMMANDED_FUEL_AIR_EQUIV_RATIO (0x44, 2),    //Fuel/Air commanded equivalence ratio                                           0 2 N/A ((A*256)+B)/32768
    THROTTLE_POS_REL               (0x45, 1),    //Relative throttle position                                                     0 100 % A*100/255
    AMBIENT_AIR_TEMP               (0x46, 1),    //Ambient air temperature                                                        -40 215 °C A-40
    THROTTLE_POS_B                 (0x47, 1),    //Absolute throttle position B                                                   0 100 % A*100/255
    THROTTLE_POS_C                 (0x48, 1),    //Absolute throttle position C                                                   0 100 % A*100/255
    THROTTLE_POS_D                 (0x49, 1),    //Accelerator pedal position D                                                   0 100 % A*100/255
    THROTTLE_POS_E                 (0x4A, 1),    //Accelerator pedal position E                                                   0 100 % A*100/255
    THROTTLE_POS_F                 (0x4B, 1),    //Accelerator pedal position F                                                   0 100 % A*100/255
    COMMANDED_THROTTLE_ACTUATOR    (0x4C, 1),    //Commanded throttle actuator                                                    0 100 % A*100/255
    RUNTIME_WITH_MIL_ON            (0x4D, 2),    //Time run with MIL on                                                           0 65535 minutes (A*256)+B
    RUNTIME_SINCE_CODES_CLEARED    (0x4E, 2),    //Time since trouble codes cleared                                               0 65535 minutes (A*256)+B
    MAX_VALUES_1                   (0x4F, 4),    //Max value for equiv ratio, o2 volt, o2 current, intake manifold abs pressure   0, 0, 0, 0 255, 255, 255, 2550 , V, mA, kPa A, B, C, D*10
    MAX_VALUES_2                   (0x50, 4),    //Maximum value for air flow rate from mass air flow sensor                      0 2550 g/s A*10, B, C, and D are reserved for future use
    FUEL_TYPE                      (0x51, 1),    //Fuel Type                                                                      From fuel type table
    FUEL_ETHANOL_PERCENT           (0x52, 1),    //Ethanol fuel %                                                                 0 100 % A*100/255
    EVAP_VAPOR_PRESSURE_ABS        (0x53, 2),    //Absolute Evap system Vapor Pressure                                            0 327.675 kPa ((A*256)+B)/200
    EVAP_VAPOR_PRESSURE_REL_2      (0x54, 2),    //Evap system vapor pressure                                                     -32767 32768 Pa ((A*256)+B)-32767
    SECONDARY_O2_SHORT_FTRIM_1_3   (0x55, 2),    //Short term secondary oxygen sensor trim bank 1 and bank 3                      -100 99.22 % (A-128)*100/128 (B-128)*100/128
    SECONDARY_O2_LONG_FTRIM_1_3    (0x56, 2),    //Long term secondary oxygen sensor trim bank 1 and bank 3                       -100 99.22 % (A-128)*100/128 (B-128)*100/128
    SECONDARY_O2_SHORT_FTRIM_2_4   (0x57, 2),    //Short term secondary oxygen sensor trim bank 2 and bank 4                      -100 99.22 % (A-128)*100/128 (B-128)*100/128
    SECONDARY_O2_LONG_FTRIM_2_4    (0x58, 2),    //Long term secondary oxygen sensor trim bank 2 and bank 4                       -100 99.22 % (A-128)*100/128 (B-128)*100/128
    FUEL_RAIL_PRESSURE_ABS         (0x59, 2),    //Fuel rail pressure (absolute)                                                  0 655350 kPa ((A*256)+B) * 10
    ACCELERATOR_PEDAL_POS_REL      (0x5A, 1),    //Relative accelerator pedal position                                            0 100 % A*100/255
    HYBRID_BATTERY_REMAINING_LIFE  (0x5B, 1),    //Hybrid battery pack remaining life                                             0 100 % A*100/255
    ENGINE_OIL_TEMP                (0x5C, 1),    //Engine oil temperature                                                         -40 210 °C A - 40
    FUEL_INJECTION_TIMING          (0x5D, 2),    //Fuel injection timing                                                          -210 301.992 ° (((A*256)+B)-26,880)/128
    ENGINE_FUEL_RATE               (0x5E, 2),    //Engine fuel rate                                                               0 3212.75 L/h ((A*256)+B)*0.05
    EMISSION_SPEC                  (0x5F, 1),    //Emission requirements to which vehicle is designed                             Bit Encoded
    DATA_PIDS_SUPPORTED_4          (0x60, 4),    //PIDs supported [61 - 80]                                                       Bit encoded [A7..D0] == [PID $61..PID $80]
    TORQUE_PERCENT_DRIVER_DEMAND   (0x61, 1),    //Driver's demand engine - percent torque                                        -125 125 % A-125
    TORQUE_PERCENT_ACTUAL          (0x62, 1),    //Actual engine - percent torque                                                 -125 125 % A-125
    TORQUE_ENGINE_REFERENCE        (0x63, 2),    //Engine reference torque                                                        0 65535 Nm A*256+B
    TORQUE_ENGINE_PERCENT          (0x64, 5),    //Engine percent torque data                                                     -125 125 % A-125 Idle B-125 Engine point 1 C-125 Engine point 2 D-125 Engine point 3 E-125 Engine point 4
    AUX_IN_OUT_SUPPORTED           (0x65, 2),    //Auxiliary input / output supported                                             Bit Encoded
    MASS_AIRFLOW_SENSOR            (0x66, 5),    //Mass air flow sensor
    ENGINE_COOLANT_TEMP_2          (0x67, 3),    //Engine coolant temperature
    INTAKE_AIR_TEMP_2              (0x68, 7),    //Intake air temperature sensor
    COMMANDED_EGR_AND_ERROR        (0x69, 7),    //Commanded EGR and EGR Error
    INTAKE_AIR_FLOW                (0x6A, 5),    //Commanded Diesel intake air flow control and relative intake air flow position
    EXAUST_RECIRCULATION_TEMP      (0x6B, 5),    //Exhaust gas recirculation temperature
    COMMANDED_THROTTLE             (0x6C, 5),    //Commanded throttle actuator control and relative throttle position
    FUEL_PRESSURE_CONTROL          (0x6D, 6),    //Fuel pressure control system
    INJECTION_PRESSURE_CONTROL     (0x6E, 5),    //Injection pressure control system
    TURBO_COMPRESSOR_INLET_PRESSURE(0x6F, 3),    //Turbocharger compressor inlet pressure
    BOOST_PRESSURE                 (0x70, 9),    //Boost pressure control
    VGT_CONTROL                    (0x71, 5),    //Variable Geometry turbo (VGT) control
    WASTEGATE_CONTROL              (0x72, 5),    //Wastegate control
    EXHAUST_PRESSURE               (0x73, 5),    //Exhaust pressure
    TURBOCHARGER_RPM               (0x74, 5),    //Turbocharger RPM
    TURBOCHARGER_TEMP_1            (0x75, 7),    //Turbocharger temperature
    TURBOCHARGER_TEMP_2            (0x76, 7),    //Turbocharger temperature
    CHANGE_AIR_COOLER_TEMP         (0x77, 5),    //Charge air cooler temperature (CACT)
    EXHAUST_GAS_TEMP_1             (0x78, 9),    //Exhaust Gas temperature (EGT) Bank 1                                           Special PID
    EXHAUST_GAS_TEMP_2             (0x79, 9),    //Exhaust Gas temperature (EGT) Bank 2                                           Special PID
    DIESEL_PARTICULATE_FILTER_1    (0x7A, 7),    //Diesel particulate filter (DPF)
    DIESEL_PARTICULATE_FILTER_2    (0x7B, 7),    //Diesel particulate filter (DPF)
    DIESEL_PARTICULATE_FILTER_TEMP (0x7C, 9),    //Diesel Particulate filter (DPF) temperature
    NOX_NTE_CONTROL_STATUS_1       (0x7D, 1),    //NOx NTE control area status
    NOX_NTE_CONTROL_STATUS_2       (0x7E, 1),    //PM NTE control area status
    ENGINE_RUNTIME                 (0x7F, 13),   //Engine run time
    DATA_PIDS_SUPPORTED_5          (0x80, 4),    //PIDs supported [81 - A0]                                                       Bit encoded [A7..D0] == [PID $81..PID $A0]
    AECD_ENGINE_RUNTIME_1          (0x81, 21),   //Engine run time for Auxiliary Emissions Control Device(AECD)
    AECD_ENGINE_RUNTIME_2          (0x82, 21),   //Engine run time for Auxiliary Emissions Control Device(AECD)
    NOX_SENSOR                     (0x83, 5),    //NOx sensor
    MAINIFOLD_SURFACE_TEMP         (0x84, 0),    //Manifold surface temperature
    NOX_REAGENT                    (0x85, 0),    //NOx reagent system
    PARTICULATE_MATTER_SENSOR      (0x86, 0),    //Particulate matter (PM) sensor
    INTAKE_MANIFOLD_PRESSURE_ABS_2 (0x87, 0),    //Intake manifold absolute pressure
    DATA_PIDS_SUPPORTED_6          (0xA0, 4),    //PIDs supported [A1 - C0]                                                       Bit encoded [A7..D0] == [PID $A1..PID $C0]
    DATA_PIDS_SUPPORTED_7          (0xC0, 4),    //PIDs supported [C1 - E0]                                                       Bit encoded [A7..D0] == [PID $C1..PID $E0]

    //PID for getting DTCs (modes 03, 07, 0A)
    GET_DTCS                       (0x00, -1, true), // n*6	Request trouble codes, 3 codes per message frame

    //PID for clearing DTCs (mode 04)
    CLEAR_DTCS                     (0x00, 0),    // Clear trouble codes / Malfunction indicator lamp (MIL) / Check engine light				Clears all stored trouble codes and turns the MIL off.


    //PIDs for Vehicle info (mode 09)
    INFO_PIDS_SUPPORTED           (0x00, 4),         // Mode 9 supported PIDs (01 to 20). Bit encoded. [A7..D0] = [PID $01..PID $20]
    VIN_MESSAGE_COUNT             (0x01, 1),         // VIN Message Count in PID 02. Only for ISO 9141-2, ISO 14230-4 and SAE J1850. Usually value will be 5
    VIN                           (0x02, 17, true),  // Vehicle Identification Number (VIN). 17-char VIN, ASCII-encoded and left-padded with null chars (0x00) if needed to
    CALIBRATION_ID_MESSAGE_COUNT  (0x03, 1),         // Calibration ID message count for PID 04. Only for ISO 9141-2, ISO 14230-4 and SAE J1850. It will be a multiple of 4 (4 messages are needed for each ID)
    CALIBRATION_ID                (0x04, 16, true),  // Calibration ID. Up to 16 ASCII chars. Data bytes not used will be reported as null bytes (0x00)
    CVN_MESSAGE_COUNT             (0x05, 1),         // Calibration verification numbers (CVN) message count for PID 06. Only for ISO 9141-2, ISO 14230-4 and SAE J1850
    CVN                           (0x06, 4),         // Calibration Verification Numbers (CVN). Raw data left-padded with null characters (0x00). Usually displayed as hex string
    PERF_TRACKING_MESSAGE_COUNT   (0x07, 1),         // In-use performance tracking message count for PID 08 and 0B. Only for ISO 9141-2, ISO 14230-4 and SAE J1850.. 8. 10. 8 if sixteen (16) values are required to be reported, 9 if eighteen (18) values are required to be reported, and 10 if twenty (20) values are required to be reported (one message reports two values, each one consisting in two bytes)
    PERF_TRACKING_SPARK           (0x08, 4),         // In-use performance tracking for spark ignition vehicles. 4 or 5 messages, each one containing 4 bytes (two values)
    ECU_NAME_MESSAGE_COUNT        (0x09, 1),         // ECU name message count for PID 0A
    ECU_NAME                      (0x0A, 20, true),  // ECU name. ASCII-coded. Right-padded with null chars (0x00)
    PERF_TRACKING_COMPRESSION     (0x0B, 4),         // In-use performance tracking for compression ignition vehicles. 5 messages, each one containing 4 bytes (two values)
    ;

    private byte code;
    /**
     * A negative bytesReturned means the number is variable.
     */
    private int bytesReturned = -1;
    private boolean isMultiline = false;

    /**
     * A list of PIDs used to query which PIDs are supported
     */
    protected static final PID[] SUPPORTED_PID_DATA_PIDS = new PID[]{
            DATA_PIDS_SUPPORTED_1, DATA_PIDS_SUPPORTED_2, DATA_PIDS_SUPPORTED_3, DATA_PIDS_SUPPORTED_4,
            DATA_PIDS_SUPPORTED_5, DATA_PIDS_SUPPORTED_6, DATA_PIDS_SUPPORTED_7
    };

    PID(int code, int bytesReturned){
        this.code = (byte) code;
        this.bytesReturned = bytesReturned;
    }

    PID(int code, int bytesReturned, boolean isMultiline){
        this(code, bytesReturned);
        this.isMultiline = isMultiline;
    }

    public byte getCode() {
        return code;
    }

    public int getBytesReturned() {
        return bytesReturned;
    }

    public boolean isMultiline() {
        return isMultiline;
    }

    @Nullable
    public static PID get(Mode mode, byte code){
        if((mode == Mode.CURRENT_DATA && code == FREEZE_DTC.getCode())
                ||(mode == Mode.FREEZE_DATA && code == MONITOR_STATUS_SINCE_CLEARED.getCode())){
            // these two PIDs are specific to their mode
            return null;
        }
        switch (mode){
            case STORED_DTCS:
            case PENDING_DTCS:
            case PERMANENT_DTCS:
                if(code == GET_DTCS.getCode()){
                    return GET_DTCS;
                }else{
                    return null;
                }
            case CLEAR_STORED_DTCS:
                if(code == CLEAR_DTCS.getCode()){
                    return CLEAR_DTCS;
                }else{
                    return null;
                }
            case CURRENT_DATA:
            case FREEZE_DATA:
                for(PID pid : values()){
                    if(pid == GET_DTCS){ //past modes 1 and two
                        return null;
                    }else if(code == pid.getCode()) {
                        return pid;
                    }
                }
                return null;
            case VEHICLE_INFO:
                int start = INFO_PIDS_SUPPORTED.ordinal();
                for(PID pid : values()){
                    if(pid.ordinal() < start){
                        continue;
                    }
                    if(code == pid.code){
                        return pid;
                    }
                }
                return null;
            default:
                return null;
        }
    }

    public String getHexCode(){
        return bytesToHex(new byte[]{code});
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

//    /**
//     * PIDs for oxygen sensor monitoring (mode 05)
//     */
//    public static class O2SensorPID {
//
//        public static final short SUPPORTED_IDS_PID = (short) 0x0100;
//
//        public static short getCode(int bank, int sensor, boolean richToLean){
//            return (short) (((richToLean ? 0x01 : 0x02) << 8) + 4*(bank-1) + sensor);
//        }
//
//        public static int getSensor(short code){
//            return (((code & 0x00FF)-1) % 4) + 1;
//        }
//
//        public static int getBank(short code){
//            return (((code & 0x00FF)-1) / 4) + 1;
//        }
//
//        /**
//         * Is sensor rich-to-lean (true) or lean-to-rich (false)
//         */
//        public static boolean isRichToLean(short code){
//            return (code >> 8) == 0x01;
//        }
//    }
}