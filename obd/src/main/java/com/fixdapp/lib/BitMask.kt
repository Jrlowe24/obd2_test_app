package com.fixdapp.lib

/**
 * Created by cjk on 3/2/17.
 */
class BitMask(private val v: Int) {

    val raw: Int get() = v

    fun contains(p: Int): Boolean {
        return v.and(p) != 0
    }
}