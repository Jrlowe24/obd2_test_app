package com.fixdapp.lib;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by cjk on 8/25/16.
 */

public class StateChangeEnforcer<T> {

    public static class Builder<T> {

        private Map<T, Set<T>> rules = new HashMap<>();
        private Set<T> fromAny = new HashSet<>();
        private Set<T> toAny = new HashSet<>();
        private boolean allowSelf = false;

        public Builder<T> allowTransition(T from, T... to){
            if(rules.get(from) == null){
                rules.put(from, new HashSet<>(Arrays.asList(to)));
            }else{
                rules.get(from).addAll(Arrays.asList(to));
            }
            return this;
        }

        public Builder<T> allowTransitionFromAnyTo(T to){
            fromAny.add(to);
            return this;
        }

        public Builder<T> allowTransitionToAnyFrom(T from){
            toAny.add(from);
            return this;
        }

        public Builder<T> allowTransitionsToSameState(boolean allow){
            allowSelf = allow;
            return this;
        }

        public Builder<T> addTransitionState(T transitionState){
            return allowTransitionFromAnyTo(transitionState).allowTransitionToAnyFrom(transitionState);
        }

        public StateChangeEnforcer<T> build(){
            return new StateChangeEnforcer<>(rules, toAny, fromAny, allowSelf);
        }
    }

    private Map<T, Set<T>> rules;
    private Set<T> fromAny;
    private Set<T> toAny;
    private boolean allowSelf;

    private StateChangeEnforcer(Map<T, Set<T>> rules, Set<T> toAny, Set<T> fromAny, boolean allowSelf){
        this.rules = rules;
        this.toAny = toAny;
        this.fromAny = fromAny;
        this.allowSelf = allowSelf;
    }

    public boolean canTransition(T from, T to){
        return fromAny.contains(to) || toAny.contains(from) ||
                (rules.get(from) != null && rules.get(from).contains(to))
                || (allowSelf && from == to);
    }

    public void enforceTransition(T from, T to){
        if(!canTransition(from,to)){
            throw new IllegalStateException("Illegal State Change: "+from+" -> "+to);
        }
    }
}
