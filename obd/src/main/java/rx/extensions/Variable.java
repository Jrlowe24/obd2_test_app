package rx.extensions;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by cjk on 8/19/16.
 *
 * Wrapper for BehaviorSubject, modeled off the class of the same name in RxSwift:
 *  https://github.com/ReactiveX/RxSwift/blob/77d72dcaee908bcf61baf88080661e748e367557/RxSwift/Subjects/Variable.swift
 */

public class Variable<T> {

    private BehaviorSubject<T> subject;

    private T value;

    public Variable(T initialValue){
        value = initialValue;
        subject = BehaviorSubject.create(initialValue);
    }

    public synchronized T getValue(){
        return value;
    }

    public synchronized void setValue(T newValue){
        value = newValue;
        subject.onNext(newValue);
    }

    public void complete() {
        subject.onCompleted();
    }

    public Observable<T> toObservable(){
        return subject.asObservable().observeOn(AndroidSchedulers.mainThread());
    }
}
