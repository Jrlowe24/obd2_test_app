package com.fixdapp.obdtestapp

import android.content.Context
import android.graphics.Typeface
import android.graphics.Typeface.NORMAL
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ScaleXSpan
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by cjk on 3/30/17.
 */
class StyledTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, private val defStyleAttr: Int = 0
) : TextView(context, attrs, defStyleAttr) {

    private val attrsArray = context.theme.obtainStyledAttributes(attrs, R.styleable.StyledTextView, defStyleAttr, 0)

    private var fontId = attrsArray.getInteger(R.styleable.StyledTextView_font, 0)
    private var kerning = attrsArray.getFloat(R.styleable.StyledTextView_kerning, 0F)

    private val typefaceCache = mutableMapOf<String?, Typeface>()

    init {
        setFont(getFont())
        setKerning(getKerning())
    }

    enum class Font(internal val path: String?) {
        default(null),
        myriadPro("fonts/myriadpro-regular.otf");
    }

    private fun getTypeface(font: Font): Typeface {
        val cached = typefaceCache[font.path]
        if (cached != null) return cached
        val face = if (font.path == null) Typeface.defaultFromStyle(defStyleAttr)
        else Typeface.createFromAsset(context.assets, font.path)
        typefaceCache[font.path] = face
        return face
    }

    fun getFont(): Font {
        return Font.values().firstOrNull { it.ordinal == fontId } ?: Font.default
    }

    fun setFont(font: Font) {
        fontId = font.ordinal
        setTypeface(getTypeface(font), NORMAL)
    }

    override fun setText(text: CharSequence?, type: BufferType) {
        super.setText(generateKerning(text), BufferType.SPANNABLE)
    }

    fun getKerning(): Float {
        return kerning
    }

    fun setKerning(kerning: Float) {
        this.kerning = kerning
        setText(getText())
    }

    /**
     * Set kerning by using the method described [here](http://stackoverflow.com/a/16429758); add
     * spaces between each character and scaleX the spaces appropriately
     */
    private fun generateKerning(text: CharSequence?): CharSequence? {
        if (text == null) return null
        val span = SpannableString(text.replace(Regex.fromLiteral("\u00A0"), "").toCharArray().joinToString("\u00A0"))
        val scale = Math.max(kerning, 0.001F)
        for (i in (1..(span.length - 2)).step(2)) {
            span.setSpan(ScaleXSpan(scale), i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        return span
    }
}