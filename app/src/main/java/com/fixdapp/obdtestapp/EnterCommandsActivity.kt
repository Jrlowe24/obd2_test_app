package com.fixdapp.obdtestapp

import android.Manifest
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.fixdapp.android.logger.DebugLoggerEndpoint
import com.fixdapp.android.logger.LogManager
import com.fixdapp.lib.obd.ELMOBDIIInterface
import com.fixdapp.lib.sensor.FIXDSensorMatcher
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensorService
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import android.text.method.ScrollingMovementMethod



class EnterCommandsActivity : AppCompatActivity() {

    val commands by lazy { findViewById(R.id.commands) as EditText }
    val results by lazy { findViewById(R.id.results) as TextView }
    val run by lazy { findViewById(R.id.run) as Button }

    var sensorService = BluetoothSensorService(FIXDSensorMatcher(),
            LogManager.getInstance().addEndpoint(DebugLoggerEndpoint()))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_commands)

        run.isEnabled = false
        run.setOnClickListener { startScan() }
        commands.setOnLongClickListener {
            commands.setText("")
            return@setOnLongClickListener true
        }

        commands.setText("ATZ\nATSP0\nATE0\nATH1\n0100\nATDP")

        results.setHorizontallyScrolling(true)
        results.movementMethod = ScrollingMovementMethod()

        sensorService.register(this)
        checkPermissions()
    }

    override fun onDestroy() {
        super.onDestroy()
        sensorService.unregister(this)
    }

    internal fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            } else {
                run.isEnabled = true
            }
        } else {
            run.isEnabled = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            run.isEnabled = true
        }
    }

    internal fun startScan() {
        run.isEnabled = false
        run.text = "..."
        Log.d("X", "searching")
        sensorService.getSensors().take(1)
                .timeout(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sensor ->
                    Log.d("X", "connected")
                    run.isEnabled = true
                    run.text = "run"
                    run.setOnClickListener { execute(sensor) }
                }, { throwable ->
                    Log.d("X", "Error: " + throwable)
                    throwable.printStackTrace()

                    Log.d("X", "clear sensor")
                    run.setOnClickListener { startScan() }
                    run.isEnabled = true
                    run.text = "search"
                })
    }

   fun execute(sensor: BluetoothSensor) {
       commands.isEnabled = false
       run.isEnabled = false
       results.text = ""
       val commandsToRun = commands.text.toString().split("\n")
       Executors.newSingleThreadExecutor().execute {
           try {
               (sensor.getInterface().toBlocking().first() as ELMOBDIIInterface).use { obdiiInterface ->
                   commandsToRun.forEach { command ->
                       if (command.startsWith("sleep")) {
                           val sleeptime = command.removePrefix("sleep ").toLong()
                           runOnUiThread { results.append("sleeping $sleeptime\n") }
                           Thread.sleep(sleeptime)
                       } else {
                           val res = obdiiInterface.sendCommand(command)
                           runOnUiThread { results.append("${res.replace("\r", "    ")}\n") }
                       }
                   }
               }
           }catch (e: Throwable) {
               runOnUiThread {
                   Log.e("X", "problem with scan", e)
                   results.append(e.message)
                   run.setOnClickListener { startScan() }
                   run.text = "search"
               }
           } finally {
               runOnUiThread {
                   commands.isEnabled = true
                   run.isEnabled = true
               }
           }
       }
   }
}
