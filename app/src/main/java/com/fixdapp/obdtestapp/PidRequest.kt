package com.fixdapp.obdtestapp

import okio.ByteString
import java.io.OutputStream
import java.io.PrintStream

/**
 * Created by cjk on 7/12/17.
 */
data class PidRequest(
        val id: Int,
        val timestamp: Long,
        val vin: String?,
        val mode: ByteString,
        val pid: ByteString?,
        val command: ByteString?,
        val address: String?,
        val protocolId: Int,
        val numberOfIterations: Int
) {

    companion object {

        fun writeToCsv(items: List<PidRequest>, stream: OutputStream) {
            val writer = PrintStream(stream, true)
            writer.println("id,timestamp,vin,mode,pid,command,address,number_of_iterations,")
            items.forEach {
                writer.println("${it.id},${it.timestamp},${it.vin ?: ""},${it.mode.hex()},${it.pid?.hex() ?: ""},${it.command?.hex() ?: ""},${it.address ?: ""},${it.numberOfIterations},")
            }
        }
    }
}

data class PidResponse(
        val requestId: Int,
        val timestamp: Long,
        val vin: String?,
        val mode: String,
        val pid: String,
        val address: String,
        val lineNumber: Int,
        val data: ByteString,
        val rawData: ByteString,
        val iteration: Int,
        val checksum: ByteString?
) {

    companion object {

        fun writeToCsv(items: List<PidResponse>, stream: OutputStream) {
            val writer = PrintStream(stream, true)
            writer.println("request_id,timestamp,vin,mode,pid,address,line_number,data,raw_data,iteration,checksum")
            items.forEach {
                writer.println("${it.requestId},${it.timestamp},${it.vin ?: ""},${it.mode},${it.pid},${it.address},${it.lineNumber},${it.data.hex()},${it.rawData.hex()},${it.iteration},${it.checksum?.hex() ?: ""}")
            }
        }
    }
}
