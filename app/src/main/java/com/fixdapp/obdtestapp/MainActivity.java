package com.fixdapp.obdtestapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fixdapp.android.logger.DebugLoggerEndpoint;
import com.fixdapp.android.logger.LogManager;
import com.fixdapp.lib.obd.ELMOBDIIInterface;
import com.fixdapp.lib.obd.OBDIIInterface;
import com.fixdapp.lib.obd.PIDResponse;
import com.fixdapp.lib.obd.exceptions.InvalidResponseException;
import com.fixdapp.lib.obd.exceptions.VehicleNotConnectedException;
import com.fixdapp.lib.sensor.FIXDSensorMatcher;
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensor;
import com.fixdapp.lib.sensor.bluetooth.BluetoothSensorService;
import com.fixdapp.lib.sensor.bluetooth.classic.BluetoothException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okio.ByteString;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView log;
    private Button run;
    private Button proto_scan;
    private ProgressBar progressBar;
    private EditText address;
    private EditText mode;
    private EditText iterations;
    private EditText fileName;
    private EditText startPidText;
    private EditText endPidText;
    private EditText incrementText;
    private EditText odometer_text;
    private EditText hexStartAddr_Text;
    private EditText hexEndAddr_Text;
    private RadioButton radioButtonStopBreakpoint;
    private Spinner scrape_type;
    private Spinner cur_protocol;
    private String hex_address;
    private String hexMode;
    private String hexStartPid;
    private String hexEndPid;
    private String hexStartAddr;
    private String hexEndAddr;
    private int incrementPids;
    private double odometer;
    private static String FULL_SCAN = "Full Scan - All Modes and Extended PIDs by Address";
    private static String FIND_MODES = "Find Valid Modes by Address";
    private static String FIND_EXT_PIDS = "Find PIDs by Address and Mode";
    private static String FIND_BROADCAST_ADDRESSES = "Find Broadcast Addresses";
    private static String RUN_PANDA_VEHICLE_ID = "Run Vehicle Identification for Panda/chffr";
    private scanType scan_mode;

    private BluetoothSensor sensor;

    BluetoothSensorService sensorService = new BluetoothSensorService(new FIXDSensorMatcher(), LogManager.getInstance().addEndpoint(new DebugLoggerEndpoint()));

    private enum scanType {
        PID, PROTOCOL
    }

    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build()); //cheat to avoid making file provider

        log = (TextView) findViewById(R.id.log);
        log.setScroller(new Scroller(this));
        log.setVerticalScrollBarEnabled(true);
        log.setMovementMethod(new ScrollingMovementMethod());

        run = (Button) findViewById(R.id.run);
        address = (EditText) findViewById(R.id.edit_address);
        hexStartAddr_Text = (EditText) findViewById(R.id.startAddr);
        hexEndAddr_Text = (EditText) findViewById(R.id.endAddr);
        mode = (EditText) findViewById(R.id.edit_mode);
        iterations = (EditText) findViewById(R.id.edit_iterations);
        fileName = (EditText) findViewById(R.id.edit_file_name);
        startPidText = (EditText) findViewById(R.id.edit_start_pid);
        endPidText = (EditText) findViewById(R.id.edit_end_pid);
        incrementText = (EditText) findViewById(R.id.edit_increment);
        odometer_text = (EditText) findViewById(R.id.edit_odometer);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        radioButtonStopBreakpoint = (RadioButton) findViewById(R.id.radioButtonStopBreakpoint);
        proto_scan = (Button) findViewById(R.id.protoScan);
        proto_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scan_mode = scanType.PROTOCOL;
                scanProtocols();
            }
        });

        cur_protocol = (Spinner) findViewById(R.id.protocol_list);
        String[] items1 = new String[]{
                "SAE_J1850_PWM",
                "SAE_J1850_VPW",
                "ISO_9141_2",
                "ISO_14230_4_KWP",
                "ISO_14230_4_KWP",
                "ISO_15765_4_CAN",
                "ISO_15765_4_CAN",
                "ISO_15765_4_CAN",
                "ISO_15765_4_CAN",
                "SAE_J1939_CAN"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items1);
        cur_protocol.setAdapter(adapter1);
        scrape_type = (Spinner) findViewById(R.id.spinner_scrape_type);
        String[] items = new String[]{
//                FIND_BROADCAST_ADDRESSES,
                FIND_EXT_PIDS,
                RUN_PANDA_VEHICLE_ID,
                FIND_MODES,
                FULL_SCAN};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        scrape_type.setAdapter(adapter);

        run.setEnabled(false);
        address.setEnabled(true);
        mode.setEnabled(true);
        iterations.setEnabled(true);
        fileName.setEnabled(true);
        startPidText.setEnabled(true);
        endPidText.setEnabled(true);
        incrementText.setEnabled(true);
        odometer_text.setEnabled(true);
        scrape_type.setEnabled(true);
        radioButtonStopBreakpoint.setEnabled(true);
        cur_protocol.setEnabled(true);
        hexStartAddr_Text.setEnabled(true);
        hexEndAddr_Text.setEnabled(true);

        run.setOnClickListener(this);
        log.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                clear();
                return true;
            }
        });
        sensorService.register(this);

        checkPermissions();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sensorService.unregister(this);
    }

    void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            } else {
                setup();
            }
        } else {
            setup();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            setup();
        }
    }

    void setup() {
        run.setEnabled(true);
        address.setEnabled(true);
        mode.setEnabled(true);
        iterations.setEnabled(true);
        fileName.setEnabled(true);
        startPidText.setEnabled(true);
        endPidText.setEnabled(true);
        incrementText.setEnabled(true);
        odometer_text.setEnabled(true);
        scrape_type.setEnabled(true);
        radioButtonStopBreakpoint.setEnabled(true);
        cur_protocol.setEnabled(true);
        hexStartAddr_Text.setEnabled(true);
        hexEndAddr_Text.setEnabled(true);
    }

    void scanProtocols() {
        if (sensor == null) {
            startScan();
        } else {
            scan_mode = scanType.PROTOCOL;
            pull();
        }

    }

    void startScan() {

        run.setEnabled(false);
        proto_scan.setEnabled(false);
        run.setText("...");
        proto_scan.setText("...");

        log("searching...");
        sensorService.getSensors().take(1)
                .timeout(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BluetoothSensor>() {
                    @Override
                    public void call(BluetoothSensor sensor) {
                        setSensor(sensor);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        log("Error: " + throwable);
                        throwable.printStackTrace();
                        clearSensor();
                    }
                });
    }

    void setSensor(BluetoothSensor sensor) {
        MainActivity.this.sensor = sensor;
        log("connected");
        run.setEnabled(true);
        run.setText("run");
        proto_scan.setText("Scan Protocols");
        mode.setEnabled(true);
        iterations.setEnabled(true);
        fileName.setEnabled(true);
        startPidText.setEnabled(true);
        endPidText.setEnabled(true);
        incrementText.setEnabled(true);
        odometer_text.setEnabled(true);
        scrape_type.setEnabled(true);
        cur_protocol.setEnabled(true);
        proto_scan.setEnabled(true);
        hexStartAddr_Text.setEnabled(true);
        hexEndAddr_Text.setEnabled(true);
    }

    void clearSensor() {
        MainActivity.this.sensor = null;
        log("clear sensor");
        run.setEnabled(true);
        run.setText("search");
        proto_scan.setText("Scan Protocols");
        mode.setEnabled(true);
        iterations.setEnabled(true);
        fileName.setEnabled(true);
        startPidText.setEnabled(true);
        endPidText.setEnabled(true);
        incrementText.setEnabled(true);
        odometer_text.setEnabled(true);
        scrape_type.setEnabled(true);
        cur_protocol.setEnabled(true);
        proto_scan.setEnabled(true);
        hexStartAddr_Text.setEnabled(true);
        hexEndAddr_Text.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        if (sensor == null) {
            startScan();
        }
//        else if ( "".equals(odometer_text.getText().toString().trim())) { log("Odometer must be set"); }
        else {
            scan_mode = scanType.PID;
            pull();
        }
    }


    void pull() {

        switch (scan_mode) {
            case PID:
                run.setText("...");
                break;
            case PROTOCOL:
                log("starting pull()");
                proto_scan.setText("...");
        }
        run.setEnabled(false);
        proto_scan.setEnabled(false);
        address.setEnabled(false);
        mode.setEnabled(false);
        startPidText.setEnabled(false);
        endPidText.setEnabled(false);
        incrementText.setEnabled(false);
        odometer_text.setEnabled(false);
        iterations.setEnabled(false);
        scrape_type.setEnabled(false);
        cur_protocol.setEnabled(false);
        hexStartAddr_Text.setEnabled(false);
        hexEndAddr_Text.setEnabled(false);
        hex_address = address.getText().toString().replace("$", "");
        hexMode = mode.getText().toString();
        hexStartPid = startPidText.getText().toString();
        hexEndPid = endPidText.getText().toString();
        hexStartAddr = hexStartAddr_Text.getText().toString();
        hexEndAddr = hexEndAddr_Text.getText().toString();


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    long start = System.currentTimeMillis();
                    log("setup sensor...");
                    try (ELMOBDIIInterface obdiiInterface = (ELMOBDIIInterface) sensor.getInterface().toBlocking().first()) {
                        log("resetting");
                        obdiiInterface.reset();
                        log(Integer.toString(cur_protocol.getFirstVisiblePosition())); //verify the jank spinner works
                        obdiiInterface.setProtocol(cur_protocol.getFirstVisiblePosition());
                        log("protocol: " + obdiiInterface.getProtocol());

//                        if(scrape_type.getSelectedItem().toString().equals(FIND_BROADCAST_ADDRESSES)){
//                            List<Integer> addresses = findValidAddresses(obdiiInterface, hex_address);
//
//                        } else
                        String vin = requestVin(obdiiInterface);

                        // default address is 7c0
                        if (hex_address != null && hex_address.length() > 0) {
                            obdiiInterface.changeRequestAddress(hex_address);
                            log("request address: " + hex_address);
                        } else {
                            log("request address: 7c0");
                            hex_address = "7c0";
                        }

                        if (scrape_type.getSelectedItem().toString().equals(RUN_PANDA_VEHICLE_ID)
                                && obdiiInterface.getProtocol().isCAN()) {
                            log("Start - " + RUN_PANDA_VEHICLE_ID);
                            requestVehicleCheck(obdiiInterface);
                            log("finished");

                        } else if (obdiiInterface.getProtocol().isCAN()) {
                            log(scrape_type.getSelectedItem().toString());
                            List<Integer> modes = new ArrayList<>();
                            int mode = 0x01;

                            switch (scan_mode) {
                                case PID:
                                    if (scrape_type.getSelectedItem().toString().equals(FULL_SCAN)
                                            || scrape_type.getSelectedItem().toString().equals(FIND_MODES)) {
                                        modes.addAll(findValidModes(obdiiInterface));
                                    } else if (!"".equals(hexMode.trim())) {
                                        mode = (int) Long.parseLong(hexMode.trim(), 16);
                                        modes.add(mode);
                                    } else {
                                        modes.add(mode);
                                    }
                                    Map<PidRequest, List<PidResponse>> requestPids = requestPids(obdiiInterface, vin, modes);
                                    if (requestPids != null && !requestPids.isEmpty()
                                            && (scrape_type.getSelectedItem().toString().equals(FULL_SCAN)
                                            || scrape_type.getSelectedItem().toString().equals(FIND_EXT_PIDS))) {

//                                  for(List<PidResponse> responses : requestPids.values()){
//                                    if (!responses.isEmpty()){
//                                        PidResponse r = responses.get(0);
//                                        log("Mode: "+r.getMode());
//                                        log("Pid: "+r.getPid());
//                                        log("Data: "+r.getRawData());
//                                        break;
//                                    }
//                                  }
                                        saveLog(requestPids);
                                    }
                                    break;
                                case PROTOCOL:
                                    Map<PidRequest, List<PidResponse>> requestDTCs = requestDTCs(obdiiInterface, vin);
                                    if (requestDTCs != null && !requestDTCs.isEmpty()) {
                                        saveLog(requestDTCs);
                                    }
                                    break;
                            }
                        } else {
                            log("Protocol is not CAN");
                        }

                    }
                    long end = System.currentTimeMillis();

                    log("took " + String.format("%.2f", (double) (end - start) / 1000 / 60.00) + " Minute(s)");
                } catch (final Exception e) {
                    if (e.getCause() instanceof BluetoothException.DeviceNoLongerConnected) {
                        log("device no longer connected");
                    } else {
                        log("ERROR: " + e);
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            clearSensor();
                        }
                    });
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        run.setEnabled(true);
                        run.setText("run");
                        address.setEnabled(true);
                        mode.setEnabled(true);
                        startPidText.setEnabled(true);
                        endPidText.setEnabled(true);
                        incrementText.setEnabled(true);
                        iterations.setEnabled(true);
                        fileName.setEnabled(true);
                        odometer_text.setEnabled(true);
                        scrape_type.setEnabled(true);
                        proto_scan.setEnabled(true);
                        proto_scan.setText("Scan Protocols");
                        cur_protocol.setEnabled(true);
                        hexStartAddr_Text.setEnabled(true);
                        hexEndAddr_Text.setEnabled(true);
                    }
                });
            }

        }).start();
    }

    private void log(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (log == null) return;
                log.append(message);
                log.append("\n");
                LogManager.getLogger().i(message);
            }
        });
    }

    private void saveLog(Map<PidRequest, List<PidResponse>> requestPids) {

        log("saving files..");
            try (ELMOBDIIInterface obdiiInterface = (ELMOBDIIInterface) sensor.getInterface().toBlocking().first()) {

                String vin = requestVin(obdiiInterface);

                DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.US);

                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).mkdirs();
                final String fileNameFormatted = (fileName.getText().toString().trim() + " ")
                        .replace(" ", "_");

                final File requestsFile = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS), fileNameFormatted + vin
                        + "_" + df.format(new Date()) + "_requests.csv");

                File responsesFile = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS), fileNameFormatted + vin
                        + "_" + df.format(new Date()) + "_responses.csv");

                FileOutputStream requestsOs = new FileOutputStream(requestsFile);
                List<PidRequest> requests = new ArrayList<>(requestPids.keySet());
                Collections.sort(requests, new Comparator<PidRequest>() {
                    @Override
                    public int compare(PidRequest o1, PidRequest o2) {
                        return o1.getId() - o2.getId();
                    }
                });
                PidRequest.Companion.writeToCsv(requests, requestsOs);
                requestsOs.close();

                FileOutputStream responsesOs = new FileOutputStream(responsesFile);
                List<PidResponse> allResponses = new ArrayList<>();
                for (List<PidResponse> a : requestPids.values()) {
                    allResponses.addAll(a);
                }
                Collections.sort(allResponses, new Comparator<PidResponse>() {
                    @Override
                    public int compare(PidResponse o1, PidResponse o2) {
                        return (o1.getRequestId() * 10 + o1.getLineNumber()) - (o2.getRequestId() * 10 + o2.getLineNumber());
                    }
                });
                PidResponse.Companion.writeToCsv(allResponses, responsesOs);
                responsesOs.close();

                final ArrayList<Uri> uris = new ArrayList<>(2);
                uris.add(Uri.fromFile(requestsFile));
                uris.add(Uri.fromFile(responsesFile));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Saved to documents", Toast.LENGTH_LONG).show();
                        final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "PID requests/responses");
                        //                                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                        startActivity(emailIntent);
                    }
                });
            } catch (Exception e) {
                if (e.getCause() instanceof BluetoothException.DeviceNoLongerConnected) {
                    log("device no longer connected");
                } else {
                    log("ERROR: " + e);
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        clearSensor();
                    }
                });
                return;
            }

    }

    public void clear() {
        log.setText("");
    }

    private String requestVin(OBDIIInterface obdiiInterface)
            throws VehicleNotConnectedException, InvalidResponseException {
        String vin = obdiiInterface.getVin();
        log("vin: " + (vin == null ? "null" : vin));
        return vin == null ? "" : vin;
    }

    private void requestVehicleCheck(OBDIIInterface obdiiInterface)
            throws VehicleNotConnectedException, InvalidResponseException {
        obdiiInterface.getVehicleCheck();
    }

    private List<Integer> findValidAddresses(OBDIIInterface obdiiInterface, String hex_address)
            throws VehicleNotConnectedException, InvalidResponseException {
        final int first_address = 0x30;
        final int last_address = 0x50;
        //obdiiInterface.setProtocol(cur_protocol.getFirstVisiblePosition();


        log("Finding valid addresses...");
        final int progress_max = (last_address - first_address + 1);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setMax(progress_max);
            }
        });
        log("Reading broadcast values from address: " + hex_address);
        StringBuffer responses = obdiiInterface.readBroadcastData(hex_address);
        String response_str = responses.toString();
        log(response_str);

        List<Integer> addresses = new ArrayList<>();

        for (int address_int = first_address; address_int <= last_address; address_int++) {
            final int progress = (address_int - first_address);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(progress);
                }
            });

            log("Reading broadcast values from address: " + address_int);
            responses = obdiiInterface.readBroadcastData(Integer.toHexString(address_int));
            response_str = responses.toString();
            log(response_str);
            if (!"".equals(response_str.trim())) {
                addresses.add(address_int);
            }

        }
        return addresses;
    }

    private Map<PidRequest, List<PidResponse>> requestDTCs(ELMOBDIIInterface obdiiInterface,
                                                           String vin)
            throws VehicleNotConnectedException, InvalidResponseException {

        String iter_str = iterations.getText().toString();
        Map<PidRequest, List<PidResponse>> results = new HashMap<>();
        obdiiInterface.setProtocol(cur_protocol.getFirstVisiblePosition()+1);

        final int first_address = 0x30;
        final int last_address = 0x50;

        List<PIDResponse> rawResponses;
        String response_str;

        log("Finding addresses...");
        final int progress_max = (last_address - first_address + 1);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setMax(progress_max);
            }
        });
        for (int address = first_address; address <= last_address; address++) {
            final int progress = (address - first_address);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(progress);
                }
            });
            log("Trying address: " + address + " on protocol: " + obdiiInterface.getProtocol());
            try {
                obdiiInterface.changeRequestAddress(Integer.toHexString(address));
                rawResponses = obdiiInterface.sendMessageHex(hexMode, hexStartPid);
                response_str = rawResponses.toString();
                log(response_str);
            } catch(IOException e) {
                log("Error: " + e + " while checking address: " + address + " on protocol: " + obdiiInterface.getProtocol());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        clearSensor();
                    }
                });

            }
        }

        return null;
    }

    private List<Integer> findValidModes(OBDIIInterface obdiiInterface)
            throws VehicleNotConnectedException, InvalidResponseException {
        final int first_mode = 0x11;
        final int last_mode = 0x35;
        final int[] pids = {0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
                0x80, 0x90, 0xA0, 0xB, 0xC0, 0xD0, 0xE0, 0xF0};

        log("Finding valid modes...");
        final int progress_max = (last_mode - first_mode + 1) * pids.length;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setMax(progress_max);
            }
        });
        List<Integer> modes = new ArrayList<>();
        String hMode;
        modeLoop:
        for (int mode = first_mode; mode <= last_mode; mode++) {
            final int progress = (mode - first_mode) * pids.length;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(progress);
                }
            });
            hMode = Integer.toHexString(mode);

            int p = 1;
            String hPid;
            for (int pid : pids) {
                hPid = Integer.toHexString(pid);
                List<PIDResponse> rawResponses = obdiiInterface.sendMessageHex(hMode, hPid, null, null);

                if (!rawResponses.isEmpty()) {
                    log("found mode: " + hMode.toUpperCase() + "\t with pid " + hPid.toUpperCase());
                    modes.add(mode);
                    continue modeLoop;
                }
                final int progress_pid = progress + p;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(progress_pid);
                    }
                });
                p++;
            }
        }
//        log("modes found: " + modes.toString());
        return modes;
    }

    private Map<Integer, List<PIDResponse>> findValidPids(OBDIIInterface obdiiInterface,
                                                          List<Integer> modes,
                                                          int startPid, int endPid,
                                                          int incrementValue)
            throws VehicleNotConnectedException, InvalidResponseException {

        final int progress_max = (endPid - startPid + 1) * modes.size();

//        log("Finding valid PIDs...");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setMax(progress_max);
            }
        });
        Map<Integer, List<PIDResponse>> pids = new HashMap<>();

        int m = 0;
        for (Integer mode : modes) {
//            final String hexMode_str = Integer.toHexString(mode);
            final int progress = (endPid - startPid) * m;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(progress);
                }
            });
            for (int pid = startPid; pid <= endPid; pid += incrementValue) {
//                List<PIDResponse> rawResponses = obdiiInterface.sendMessageHex(hexMode_str, Integer.toHexString(pid), null, null);
//                if (! rawResponses.isEmpty()) {
//                    pids.put(pid, rawResponses);
//                }
                pids.put(pid, null);
                final int progress_pid = (pid - startPid + 1) + progress;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(progress_pid);
                    }
                });
            }
            m++;
        }
        return pids;
    }

    private Map<PidRequest, List<PidResponse>> requestPids(OBDIIInterface obdiiInterface,
                                                           String vin, List<Integer> modes)
            throws VehicleNotConnectedException, InvalidResponseException {

        String iter_str = iterations.getText().toString();
        Map<PidRequest, List<PidResponse>> results = new HashMap<>();
        int iter = 30;

        if (!"".equals(iter_str.trim()) && Integer.parseInt(iter_str) > 0) {
            iter = Integer.parseInt(iter_str);
        }
        final int iterations = iter;

        // static value threshold - value that determines when to stop looping on pid
        int staticValueThreshold = 6 < iterations ? 6 : iterations;

        if (scrape_type.getSelectedItem().toString().equals(FULL_SCAN)
                || scrape_type.getSelectedItem().toString().equals(FIND_EXT_PIDS)) {

            int startPid = hexStartPid.trim().length() > 0 ? (int) Long.parseLong(hexStartPid.trim(), 16) : 0x00;
            int endPid = hexEndPid.trim().length() > 0 ? (int) Long.parseLong(hexEndPid.trim(), 16) : 0xFF;
            String incrementTemp = incrementText.getText().toString();
            incrementPids = incrementTemp.length() > 0 ? Integer.parseInt(incrementTemp) : 1;
            log("Searching for PIDs between " + Integer.toHexString(startPid).toUpperCase()
                    + " and " + Integer.toHexString(endPid).toUpperCase() + " in increments of "
                    + incrementPids);

            Map<Integer, List<PIDResponse>> pids = findValidPids(obdiiInterface, modes,
                    startPid, endPid, incrementPids);

            if (pids.isEmpty()) {
                log("no PIDs to scrape");
                return null;
            } else {
                log("total PIDs: " + pids.size());
            }

            log("Starting PID scrape...");
            int pids_static = 0;
            int pids_continuous = 0;
            int no_response = 0;
            final int progress_max = modes.size() * pids.size() * iterations;
//            log("progress_max - "+progress_max);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setMax(progress_max);
                }
            });

            int m = 0;
            for (Integer mode : modes) {
                final int progress = m * (pids.size() * iterations);
//                log("progress 1 - "+progress);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(progress);
                    }
                });

                String hexModeStr = Integer.toHexString(mode);
                hexModeStr = hexModeStr.trim().length() % 2 == 0 ? hexModeStr.trim() : "0" + hexModeStr.trim();
                int i = 0;

                pidLoop:
                for (int pid : pids.keySet()) {
                    final int progress_pids = progress + (i * iterations);
                    log("  pid - " + Integer.toHexString(pid).toUpperCase());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progress_pids);
                        }
                    });

                    String hexPidStr = Integer.toHexString(pid);
                    hexPidStr = hexPidStr.trim().length() % 2 == 0 ? hexPidStr.trim() : "0" + hexPidStr.trim();

                    Map<String, Integer> addressLineUniqueCounter = new HashMap<>(20);
                    Map<PidRequest, List<PidResponse>> tempResults = new HashMap<>(300);
                    PidRequest firstRequest = null;
                    List<PidResponse> firstResponse = null;

                    for (int iteration = 0; iteration < iterations; iteration++) {
                        final int progress_iter = progress_pids + iteration;
//                        log("progress 3 - "+progress_iter);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setProgress(progress_iter);
                            }
                        });

                        PidRequest request = new PidRequest(progress_iter, System.currentTimeMillis(), vin,
                                ByteString.decodeHex(hexModeStr), ByteString.decodeHex(hexPidStr),
                                null, null, obdiiInterface.getProtocol().ordinal() + 1, iteration);

                        List<PIDResponse> rawResponses = obdiiInterface.sendMessageHex(hexModeStr, hexPidStr, null, null);


//                        if(iteration == 1 && pids.containsKey(pid)){
//                            int matchCount = 0;
//                            for(PIDResponse initScanPR :  pids.get(pid)){
//                                if (addressLineUniqueCounter.containsKey(initScanPR.toString())) {
////                                    matchCount += addressLineUniqueCounter.get(initScanPR.toString());
//                                    pids_static++;
//                                    i++;
//                                    results.put(firstRequest, firstResponse);
//                                    continue pidLoop;
//                                }
//                            }
//                        }

                        String key = "empty";
                        if (iteration == staticValueThreshold) {
                            for (String k : addressLineUniqueCounter.keySet()) {
//                                log("value: "+value+", threshold:"+staticValueThreshold);
                                if (addressLineUniqueCounter.get(k) >= staticValueThreshold) {
                                    if (k.equals(key)) {
                                        no_response++;
                                    } else {
                                        pids_static++;
                                        results.put(firstRequest, firstResponse);
                                    }
                                    i++;
                                    continue pidLoop;
                                }
                            }
                            pids_continuous++;
                        }

                        if (rawResponses.isEmpty()) {
                            if (addressLineUniqueCounter.containsKey(key)) {
                                addressLineUniqueCounter.put(key, 1 + addressLineUniqueCounter.get(key));
                            } else {
                                addressLineUniqueCounter.put(key, 1);
                            }
                            continue;
                        }

                        Long responseTime = System.currentTimeMillis();
                        List<PidResponse> responses = new ArrayList<>(rawResponses.size());
                        for (PIDResponse rawResponse : rawResponses) {
                            int j = 0;

                            if (iteration <= staticValueThreshold) {
                                key = rawResponse.getResponderId() + rawResponse.getData().toString();
                                if (addressLineUniqueCounter.containsKey(key)) {
                                    addressLineUniqueCounter.put(key, 1 + addressLineUniqueCounter.get(key));
                                } else {
                                    addressLineUniqueCounter.put(key, 1);
                                }
                            }
                            for (byte[] line : rawResponse.getRawResponses()) {
                                if (j == 0) {
                                    log("iter: " + iteration
                                            + "\t - pid: " + hexPidStr
                                            + "\t - rawResponse: " + rawResponse.getData().toString());
                                }
//                                log("getReturnedMode - "+rawResponse.getReturnedMode());
                                responses.add(new PidResponse(
                                        progress_iter,
                                        responseTime,
                                        vin,
                                        rawResponse.getReturnedMode(),
                                        rawResponse.getReturnedPID(),
                                        rawResponse.getResponderId(),
                                        j,
                                        rawResponse.getData().toByteString(),
                                        ByteString.of(line),
                                        iteration,
                                        rawResponse.getChecksums() == null ? null : ByteString.of(rawResponse.getChecksums().get(j))
                                ));
                                j++;
                            }
                            if (iteration == 0) {
                                firstRequest = request;
                                firstResponse = responses;
                            }
                        }
                        tempResults.put(request, responses);
                    }
                    i++;
                    results.putAll(tempResults);
                    tempResults.clear();
                }
                m++;
                log("Finished - Static PIDs: " + pids_static + ", Continuous PIDs: " + pids_continuous);
                log(", No Resp PIDs: " + no_response);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(progress_max);
//                    clear();
                }
            });
        }

        return results;

        /*
         * 18DAF110  06 41 01   86 07 EF 80
         * */
    }
}
